import React from 'react';
import {SafeAreaView, StatusBar} from 'react-native';
import SplashScreen from 'react-native-splash-screen';
import {t} from 'react-native-tailwindcss';
import AppRoute from './AppRoute';

const App = () => {
  React.useEffect(() => {
    SplashScreen.hide();
  }, []);
  return (
    <SafeAreaView style={[t.flex1, t.bgWhite]} forceInset={{top: 'never'}}>
      <StatusBar barStyle={'light-content'} />
      <AppRoute />
    </SafeAreaView>
  );
};

export default App;
