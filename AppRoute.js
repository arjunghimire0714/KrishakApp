import {useNetInfo} from '@react-native-community/netinfo';
import messaging from '@react-native-firebase/messaging';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Auth, Login, Register} from '@screens/Auth';
import Main from '@screens/Main';
import Training from '@screens/Training';
import Participation from '@screens/Training/Participation';
import * as React from 'react';
import {ActivityIndicator, Linking, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {useDispatch, useSelector} from 'react-redux';
import Connection from './src/components/shared/Connection';
import {
  HomeActiveIcon,
  HomeIcon,
  NotificationsActiveIcon,
  NotificationsIcon,
  ProfileActiveIcon,
  ProfileIcon,
} from './src/images';
import {navigationRef} from './src/navigation';
import {authActions} from './src/redux/actions';
import ForgotPassword from './src/screens/Auth/ForgotPassword';
import NewPassword from './src/screens/Auth/NewPassword';
import Verify from './src/screens/Auth/Verify';
import Contests from './src/screens/Contests';
import Correct from './src/screens/Contests/Correct';
import InCorrect from './src/screens/Contests/InCorrect';
import Timer from './src/screens/Contests/Timer';
import Weekly from './src/screens/Contests/Weekly';
import Winner from './src/screens/Contests/Winner';
import Notifications from './src/screens/Main/Notifications';
import Profile from './src/screens/Main/Profile';
import Suggestions from './src/screens/Suggestions';
import DetailsSuggestions from './src/screens/Suggestions/Details';
import SearchSuggestions from './src/screens/Suggestions/Search';
import OldTraining from './src/screens/Training/OldTraining';
import Success from './src/screens/Training/Success';

const WIDTH = 25;
const HEIGHT = 25;

const Tab = createBottomTabNavigator();

const Stack = createNativeStackNavigator();

const TrainingStack = createNativeStackNavigator();
const ContestsStack = createNativeStackNavigator();
const SuggestionsStack = createNativeStackNavigator();

const SuggestionsStackScreens = () => (
  <SuggestionsStack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <SuggestionsStack.Screen name="Home" component={Suggestions} />
    <SuggestionsStack.Screen
      name="SearchSuggestions"
      component={SearchSuggestions}
    />
    <SuggestionsStack.Screen
      name="DetailsSuggestions"
      component={DetailsSuggestions}
    />
  </SuggestionsStack.Navigator>
);

const ContestsStackScreens = () => (
  <ContestsStack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <ContestsStack.Screen name="Home" component={Contests} />
    <ContestsStack.Screen name="Correct" component={Correct} />
    <ContestsStack.Screen name="InCorrect" component={InCorrect} />
    <ContestsStack.Screen name="Timer" component={Timer} />
  </ContestsStack.Navigator>
);

const TrainingStackScreens = () => (
  <TrainingStack.Navigator
    screenOptions={{
      headerShown: false,
    }}>
    <TrainingStack.Screen name="Home" component={Training} />
    <TrainingStack.Screen name="OldTraining" component={OldTraining} />
    <TrainingStack.Screen name="Participation" component={Participation} />
    <TrainingStack.Screen name="Success" component={Success} />
    <TrainingStack.Screen name="Winner" component={Winner} />
  </TrainingStack.Navigator>
);

const HomeStackScreens = () => (
  <Tab.Navigator
    initialRouteName="Home"
    screenOptions={{
      headerShown: false,
      style: {
        height: 60,
        paddingTop: 10,
        paddingBottom: 8,
      },
      labelStyle: [t.fontM],
    }}>
    <Tab.Screen
      name="Home"
      component={Main}
      options={{
        tabBarShowLabel: false,
        tabBarIcon: ({focused}) =>
          focused ? (
            <HomeActiveIcon width={WIDTH} height={HEIGHT} />
          ) : (
            <HomeIcon width={WIDTH} height={HEIGHT} />
          ),
      }}
    />
    <Tab.Screen
      name="Notifications"
      component={Notifications}
      options={{
        tabBarShowLabel: false,
        tabBarIcon: ({focused}) =>
          focused ? (
            <NotificationsActiveIcon width={WIDTH} height={HEIGHT} />
          ) : (
            <NotificationsIcon width={WIDTH} height={HEIGHT} />
          ),
      }}
    />
    <Tab.Screen
      name="Profile"
      component={Profile}
      options={{
        tabBarShowLabel: false,
        tabBarIcon: ({focused}) =>
          focused ? (
            <ProfileActiveIcon width={WIDTH} height={HEIGHT} />
          ) : (
            <ProfileIcon width={WIDTH} height={HEIGHT} />
          ),
      }}
    />
  </Tab.Navigator>
);

const linking = {
  prefixes: [
    'https://*.offgridbazaar.com',
    'https://superkrishak.offgridbazaar.com',
  ],
};
function App() {
  const dispatch = useDispatch();
  const [, setToken] = React.useState('');
  const netInfo = useNetInfo();

  const {isLoggedUser, checkLoggedUser} = useSelector(state => state.auth);
  const deepLinking = async () => {
    const initialUrl = await Linking.getInitialURL();
    if (initialUrl) {
      dispatch({
        type: authActions.GET_RESET_URL_REQUEST,
        data: initialUrl,
      });
    }

    Linking.addEventListener('url', ({url}) => {
      dispatch({
        type: authActions.GET_RESET_URL_REQUEST,
        data: url,
      });
    });
  };

  React.useEffect(() => {
    deepLinking();
  }, []);

  React.useEffect(() => {
    if (isLoggedUser) {
      messaging().requestPermission({
        sound: true,
        badge: true,
        alert: true,
      });
    }
  }, [isLoggedUser]);

  React.useEffect(() => {
    if (isLoggedUser) {
      // get the device token on app load
      messaging()
        .getToken()
        .then(tok => {
          setToken(tok);
          dispatch({
            type: authActions.REGISTER_NOTIFICATION_TOKEN_REQUEST,
            data: tok,
          });
        });
      return messaging().onTokenRefresh(tok => {
        setToken(tok);
        dispatch({
          type: authActions.REGISTER_NOTIFICATION_TOKEN_REQUEST,
          data: tok,
        });
      });
    }
  }, [isLoggedUser]);

  if (checkLoggedUser) {
    return (
      <View style={[t.flex1, t.justifyCenter, t.itemsCenter]}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    );
  }

  return (
    <>
      {!netInfo.isConnected && <Connection />}
      <NavigationContainer linking={linking} ref={navigationRef}>
        <Stack.Navigator
          screenOptions={{
            headerShown: false,
          }}>
          {isLoggedUser ? (
            <>
              <Stack.Screen name="Main" component={HomeStackScreens} />
              <Stack.Screen name="Training" component={TrainingStackScreens} />
              <Stack.Screen name="Contests" component={ContestsStackScreens} />
              <Stack.Screen
                name="Suggestions"
                component={SuggestionsStackScreens}
              />
              <Stack.Screen name="Participation" component={Participation} />
              <Stack.Screen
                name="DetailsSuggestions"
                component={DetailsSuggestions}
              />
              <Stack.Screen name="Success" component={Success} />
              <Stack.Screen name="Winner" component={Winner} />
              <Stack.Screen name="Weekly" component={Weekly} />
            </>
          ) : (
            <>
              <Stack.Screen name="Auth" component={Auth} />
              <Stack.Screen name="Login" component={Login} />
              <Stack.Screen name="Register" component={Register} />
              <Stack.Screen name="Verify" component={Verify} />
              <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
              <Stack.Screen name="NewPassword" component={NewPassword} />
            </>
          )}
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

export default App;
