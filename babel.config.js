module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        extensions: ['.ios.js', '.android.js', '.js', '.json'],
        alias: {
          '@components': './src/components',
          '@helpers': './src/helpers',
          '@images': './src/images',
          '@screens': './src/screens',
          '@redux': './src/redux',
          '@navigation': './src/navigation',
        },
      },
    ],
  ],
};
