import 'react-native-url-polyfill/auto';
import store from '@redux/store';
import React from 'react';
import {AppRegistry} from 'react-native';
import {ReduxNetworkProvider} from 'react-native-offline';
import {RootSiblingParent} from 'react-native-root-siblings';
import {Provider} from 'react-redux';
import App from './App';
import {name as appName} from './app.json';
import messaging from '@react-native-firebase/messaging';

messaging().setBackgroundMessageHandler(async remoteMessage => {
  console.log('Message received in the background!', remoteMessage);
});

const MainApp = () => (
  <RootSiblingParent>
    <Provider store={store}>
      <ReduxNetworkProvider>
        <App />
      </ReduxNetworkProvider>
    </Provider>
  </RootSiblingParent>
);

AppRegistry.registerComponent(appName, () => MainApp);
