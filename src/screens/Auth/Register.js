import Button from '@components/shared/Button';
import Loader from '@components/shared/Loader';
import Text from '@components/shared/Text';
import TextInput from '@components/shared/TextInput';
import {appId, webClientId} from '@helpers';
import {signupSchema} from '@helpers/validation';
import {FacebookLogin, GoogleLogin} from '@images';
import {GoogleSignin} from '@react-native-community/google-signin';
import {authActions} from '@redux/actions';
import {useFormik} from 'formik';
import React, {useEffect} from 'react';
import {TouchableOpacity, View} from 'react-native';
import {AccessToken, LoginManager, Settings} from 'react-native-fbsdk-next';
import PTRView from 'react-native-pull-to-refresh';
import {t} from 'react-native-tailwindcss';
import {useDispatch, useSelector} from 'react-redux';

Settings.setAppID(appId);

const initialValues = {
  name: '',
  email: '',
  password: '',
};

const Register = ({navigation: {navigate}}) => {
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const {authLoading} = useSelector(state => state.auth);

  const dispatch = useDispatch();

  const handleSignUp = data => {
    dispatch({type: authActions.REGISTER_REQUEST, data});
  };

  const handleIconPress = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const initialize = async () => {
    await GoogleSignin.configure({
      webClientId,
      forceConsentPrompt: true,
    });
  };

  const handleGoogleLogin = async () => {
    try {
      await GoogleSignin.signOut();
      const {idToken} = await GoogleSignin.signIn();
      dispatch({
        type: authActions.LOGIN_REQUEST,
        data: {
          tokenId: idToken,
          type: 'google',
        },
      });
      await GoogleSignin.signOut();
    } catch (error) {
      await GoogleSignin.signOut();
      console.log('ERROR GOOGLE', error);
    }
  };

  const handleFacebookLogin = () => {
    try {
      LoginManager.logOut();
      LoginManager.logInWithPermissions(['public_profile', 'email']).then(
        res => {
          if (res.isCancelled) {
          } else {
            AccessToken.getCurrentAccessToken().then(data => {
              dispatch({
                type: authActions.LOGIN_REQUEST,
                data: {
                  auth_token: data.accessToken,
                  type: 'facebook',
                },
              });
              LoginManager.logOut();
            });
          }
        },
      );
    } catch (e) {
      LoginManager.logOut();
    }
  };

  const formik = useFormik({
    validationSchema: signupSchema,
    initialValues,
    onSubmit: handleSignUp,
  });
  const {values, errors, handleChange, handleBlur, handleSubmit, setValues} =
    formik;

  const handleRefresh = () => {
    setValues(initialValues);
    dispatch({
      type: authActions.RESET_MESSAGE_REQUEST,
    });
  };

  useEffect(() => {
    initialize();
  }, []);

  return (
    <PTRView style={[t.bgWhite, t.flex1]} onRefresh={handleRefresh}>
      <View style={[t.flex1, t.bgWhite, t.p4]}>
        <Text style={[t.text3xl, t.fontB, t.textCenter, t.textBlack]}>
          कृषक खाता सिर्जना
        </Text>
        <Loader loading={authLoading} />
        <View style={[]}>
          <TouchableOpacity
            onPress={handleFacebookLogin}
            style={[
              t.mY2,
              t.bgFacebook,
              t.flexRow,
              t.itemsCenter,
              t.p2,
              t.roundedLg,
            ]}>
            <FacebookLogin />
            <Text style={[t.textLg, t.mL2, t.fontB, t.textCenter, t.textWhite]}>
              फेसबुकबाट खाता सिर्जना गर्नुहोस्
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={handleGoogleLogin}
            style={[
              t.mY2,
              t.bgGoogle,
              t.flexRow,
              t.itemsCenter,
              t.p2,
              t.roundedLg,
            ]}>
            <GoogleLogin />
            <Text style={[t.textLg, t.mL2, t.fontB, t.textCenter, t.textWhite]}>
              गुगलबाट खाता सिर्जना गर्नुहोस्
            </Text>
          </TouchableOpacity>
        </View>
        <View style={[t.mY6]}>
          <Text style={[t.fontSb, t.textCenter, t.textText]}>
            अथवा आफ्नो फोन वा इमेलबाट खाता सिर्जना गर्नुहोस्
          </Text>
          <View style={[t.mY2]}>
            <View style={[t.mB4]}>
              <TextInput
                onChangeText={handleChange('name')}
                onBlur={handleBlur('name')}
                value={values.name}
                placeholder="नाम"
                style={[t.fontSb, t.borderT, t.roundedTLg]}
                errorMessage={errors.name}
              />
              <TextInput
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                value={values.email}
                placeholder="इमेल"
                errorMessage={errors.email}
              />
              <TextInput
                onChangeText={handleChange('password')}
                onBlur={handleBlur('password')}
                showPassword
                secureTextEntry={secureTextEntry}
                value={values.password}
                placeholder="पासवर्ड"
                handleIconPress={handleIconPress}
                style={[t.roundedBLg, t.fontSb]}
                errorMessage={errors.password}
              />
            </View>

            <Button
              // disabled={isValid}
              onPress={handleSubmit}
              title="खाता सिर्जना गर्नुहोस्"
            />
            <View
              style={[
                t.flex,
                t.justifyCenter,
                t.itemsCenter,
                t.flexRow,
                t.mY2,
              ]}>
              <Text style={[t.textText, t.textLg, t.fontSb]}>
                कृषक खाता छ ?
              </Text>
              <Button
                onPress={() => navigate('Login')}
                btnStyle={[t.bgWhite, t.mL2, t.p0]}
                textStyle={[t.textColor1, t.textBase]}
                title="साइन इन गर्नुहोस्"
              />
            </View>
          </View>
        </View>
      </View>
    </PTRView>
  );
};

export default Register;
