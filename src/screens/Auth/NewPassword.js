import Button from '@components/shared/Button';
import Text from '@components/shared/Text';
import TextInput from '@components/shared/TextInput';
import {authActions} from '@redux/actions';
import {useFormik} from 'formik';
import React from 'react';
import {ScrollView, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {useDispatch} from 'react-redux';
import {newPasswordSchema} from '@helpers/validation';

const initialValues = {
  password: '',
  confirm: '',
};

const NewPassword = () => {
  const dispatch = useDispatch();
  const [secureTextEntry1, setSecureTextEntry1] = React.useState(true);
  const [secureTextEntry2, setSecureTextEntry2] = React.useState(true);
  const handleChangePassword = data => {
    dispatch({
      type: authActions.RESET_PASSWORD_REQUEST,
      data,
    });
  };

  const formik = useFormik({
    validationSchema: newPasswordSchema,
    initialValues,
    onSubmit: handleChangePassword,
  });
  const {values, errors, handleChange, handleBlur, handleSubmit} = formik;

  return (
    <ScrollView contentContainerStyle={[t.flexGrow]} style={[t.bgWhite]}>
      <View style={[t.flex1, t.bgWhite, t.p4]}>
        <Text style={[t.text3xl, t.fontSb, t.textCenter, t.textBlack]}>
          पासवर्ड फेर्नुहोस्
        </Text>

        <View style={[t.mY2, t.flex1]}>
          <Text style={[t.fontSb, t.textCenter, t.textText]}>
            अथवा आफ्नो फोन वा इमेलबाट साइन इन गर्नुहोस्
          </Text>
          <View style={[t.mY2, t.flex1, t.flexCol, t.justifyBetween]}>
            <View>
              <View style={[t.mB4]}>
                <TextInput
                  handleIconPress={() => setSecureTextEntry1(!secureTextEntry1)}
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                  value={values.password}
                  secureTextEntry={secureTextEntry1}
                  showPassword
                  placeholder="नया पासवर्ड"
                  style={[t.roundedTLg, t.fontSb]}
                  errorMessage={errors.password}
                />
                <TextInput
                  handleIconPress={() => setSecureTextEntry2(!secureTextEntry2)}
                  onChangeText={handleChange('confirm')}
                  onBlur={handleBlur('confirm')}
                  value={values.confirm}
                  secureTextEntry={secureTextEntry2}
                  showPassword
                  placeholder="नया पासवर्ड दोहोर्याउनुहोस्"
                  style={[t.roundedBLg, t.borderB, t.fontSb]}
                  errorMessage={errors.password}
                />
              </View>
            </View>
            {(errors.password || errors.confirm) && (
              <View
                style={[
                  t.flex,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.flexRow,
                  t.mY2,
                  t.bgBgColor2,
                  t.p3,
                  t.roundedLg,
                ]}>
                <Text style={[t.textColor6, t.textBase, t.fontSb]}>
                  {errors.password || errors.confirm}
                </Text>
              </View>
            )}
            <Button
              // disabled={!isValid}
              onPress={handleSubmit}
              title="अगाडि बढ्नुहाेस्"
            />
          </View>
        </View>
      </View>
    </ScrollView>
  );
};

export default NewPassword;
