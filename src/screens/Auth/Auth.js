import Text from '@components/shared/Text';
import {LogoSvg} from '@images';
import React from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import {t} from 'react-native-tailwindcss';

const Auth = ({navigation: {navigate}}) => {
  return (
    <View style={[t.flex1]}>
      <View style={[t.flex1]}>
        <View
          style={[
            t.bgColor1,
            {
              flex: 0.3,
            },
          ]}></View>
        <View
          style={[{flex: 0.7}, t.bgBgColor3, t.itemsCenter, t.justifyBetween]}>
          <View
            style={[
              t.itemsCenter,
              {
                flex: 0.6,
              },
              t._mT20,
            ]}>
            <LogoSvg />
          </View>
          <View style={[t.itemsCenter, {flex: 0.4}]}>
            <Text style={[t.textBase, t.fontSb, t.textCenter, t.textWhite]}>
              सुपर कृषक काे यात्रामा यहाँहरूलाइ स्वागत छ...
            </Text>
            <Text
              style={[t.text2xl, t.fontSb, t.mY2, t.textCenter, t.textWhite]}>
              के तपाईको कृषक खाता छ?
            </Text>
            <View style={[t.flex, t.flexRow, t.mT4]}>
              <TouchableOpacity
                onPress={() => navigate('Login')}
                style={[
                  t.border2,
                  t.roundedLg,
                  t.w16,
                  t.h10,
                  t.borderWhite,
                  t.mR4,
                  t.justifyCenter,
                  t.itemsCenter,
                ]}>
                <Text style={[t.fontB, t.textXl, t.textWhite]}>छ</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => navigate('Register')}
                style={[
                  t.border2,
                  t.mL4,
                  t.roundedLg,
                  t.w16,
                  t.h10,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.borderWhite,
                ]}>
                <Text style={[t.fontB, t.textXl, t.textWhite]}>छैन</Text>
              </TouchableOpacity>
            </View>
            <View style={[t.flex, t.flexRow, t.itemsCenter]}>
              <Image
                style={[t.w20, t.h20, t.roundedLg, t.mB2]}
                resizeMode="contain"
                source={require('@images/tradelogo.png')}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default Auth;
