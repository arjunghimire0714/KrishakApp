import Button from '@components/shared/Button';
import Text from '@components/shared/Text';
import {authActions} from '@redux/actions';
import React, {useState} from 'react';
import {ScrollView, View} from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';
import {t} from 'react-native-tailwindcss';
import {useDispatch, useSelector} from 'react-redux';

const Verify = ({navigation: {navigate}}) => {
  const [code, setCode] = useState(undefined);
  const {signUpData, authLoading} = useSelector(state => state.auth);

  const dispatch = useDispatch();
  const handleVerify = () => {
    const data = {
      otp_code: code,
      email: signUpData?.email,
    };
    dispatch({type: authActions.VERIFY_REQUEST, data});
  };
  const disabled = code?.length === 4;

  return (
    <ScrollView style={[t.bgWhite]}>
      <View style={[t.flex1, t.bgWhite, t.p4]}>
        <View>
          <Text style={[t.text3xl, t.fontSb, t.textCenter, t.textBlack]}>
            OTP दिनुहोस्
          </Text>

          <View style={[t.mY3]}>
            <Text style={[t.fontSb, t.textCenter, t.textText]}>
              तपाईको इमेल वा फोनमा आएको OTP दिनुहोस्
            </Text>
            <View style={[t.mY2]}>
              <View style={[t.mB4]}>
                <CodeInput
                  codeLength={4}
                  keyboardType="numeric"
                  activeColor="#000"
                  inactiveColor="#E2E5E9"
                  autoFocus={false}
                  ignoreCase={true}
                  inputPosition="center"
                  size={50}
                  codeInputStyle={[t.roundedLg]}
                  onFulfill={value => setCode(value)}
                />
              </View>
            </View>
          </View>
        </View>

        <Button onPress={handleVerify} title="अगाडि बढ्नुहाेस्" />
      </View>
    </ScrollView>
  );
};

export default Verify;
