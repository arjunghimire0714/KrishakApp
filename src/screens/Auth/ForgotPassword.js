import Button from '@components/shared/Button';
import Text from '@components/shared/Text';
import TextInput from '@components/shared/TextInput';
import {authActions} from '@redux/actions';
import {useFormik} from 'formik';
import React from 'react';
import {ScrollView, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {useDispatch, useSelector} from 'react-redux';
import {forgotPasswordSchema} from '@helpers/validation';

const initialValues = {
  // email: 'admin@superkrishak.com',
  email: '',
};

const ForgotPassword = () => {
  const dispatch = useDispatch();
  const {message} = useSelector(state => state.auth);
  const handleForgotPassword = data => {
    dispatch({type: authActions.FORGOT_PASSWORD_REQUEST, data});
  };
  const formik = useFormik({
    validationSchema: forgotPasswordSchema,
    initialValues,
    onSubmit: handleForgotPassword,
  });
  const {values, errors, isValid, handleChange, handleBlur, handleSubmit} =
    formik;

  return (
    <ScrollView contentContainerStyle={[t.flexGrow]} style={[t.bgWhite]}>
      <View style={[t.flex1, t.bgWhite, t.p4]}>
        <Text style={[t.text3xl, t.fontSb, t.textCenter, t.textBlack]}>
          पासवर्ड बिर्सिनुभयो?
        </Text>
        <View style={[t.mY6, t.flex1, t.flexCol, t.justifyBetween]}>
          <View style={[t.mY2]}>
            <View style={[t.mB4]}>
              <TextInput
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                value={values.email}
                placeholder="फोन / इमेल"
                style={[t.roundedLg, t.fontSb]}
                errorMessage={errors.email}
              />
            </View>
            {message && (
              <View
                style={[
                  t.flex,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.flexRow,
                  t.mY2,
                  t.bgBgColor2,
                  t.p3,
                  t.roundedLg,
                ]}>
                <Text style={[t.textColor6, t.textBase, t.fontSb]}>
                  {message}
                </Text>
              </View>
            )}
          </View>
          <Button
            disabled={!isValid}
            onPress={handleSubmit}
            title="अगाडि बढ्नुहाेस्"
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default ForgotPassword;
