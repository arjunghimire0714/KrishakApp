import Button from '@components/shared/Button';
import Header from '@components/shared/Header';
import Text from '@components/shared/Text';
import {$axios} from '@helpers/axios';
import CustomToast from '@helpers/toast';
import {URLS} from '@helpers/urls';
import {trainingSchema} from '@helpers/validation';
import {useFormik} from 'formik';
import React, {useRef, useState} from 'react';
import {ScrollView, TouchableOpacity, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import Wizard from 'react-native-wizard';
import Loader from '../../components/shared/Loader';
import Form1 from './form/Form1';
import Form2 from './form/Form2';
import Form3 from './form/Form3';
import Form4 from './form/Form4';

export default function Participation({route, navigation: {navigate}}) {
  const wizard = useRef();
  const {trainingId} = route.params;
  const [loading, setLoading] = useState(false);
  const [, setIsFirstStep] = useState();
  const [, setIsLastStep] = useState();
  const [currentStep, setCurrentStep] = useState(0);

  const handleParticipationForm = async data => {
    try {
      setLoading(true);
      let sendData;
      if (data?.profession?.includes('अन्य (कृपया व्यवसाय खुलाउनुहोला)')) {
        sendData = {
          ...data,
          profession: [...data.profession, data.other].filter(
            profession => profession !== 'अन्य (कृपया व्यवसाय खुलाउनुहोला)',
          ),
        };
      } else {
        sendData = {
          ...data,
        };
      }
      const registerResponse = await $axios.post(
        URLS.TRAINING_REGISTRATION_URL(trainingId),
        sendData,
      );
      setLoading(false);
      if (registerResponse?.data) {
        navigate('Success');
      }
    } catch (error) {
      console.log('ERROR', error.response.data, data);
      CustomToast('केहि गलत भयो');
      setLoading(false);
    }
  };
  const initialValues = {
    name: '',
    province: '',
    district: '',
    body: '',
    ward_no: '',
    profession: [],
    email: '',
    contact_no: '',
    other: '',
  };
  const formik = useFormik({
    validationSchema: trainingSchema,
    initialValues,
    onSubmit: handleParticipationForm,
  });
  const {
    values,
    errors,
    handleChange,
    handleBlur,
    handleSubmit,
    setFieldError,
    setFieldValue,
  } = formik;

  const handleChangeProfession = (value, item) => {
    if (values.profession?.includes(item)) {
      setFieldValue(
        'profession',
        values.profession?.filter(i => i !== item),
      );
    } else {
      setFieldValue('profession', [...values.profession, item]);
    }
  };

  const steps = [
    {
      id: 1,
      title: 'व्यक्तिगत विवरण',
      description:
        'आफ्नो सिट बुक गर्न र सहभागिता पक्का गर्न तलको फर्म भर्नुहोस्',
      component: (
        <Form1
          values={values}
          errors={errors}
          handleChange={handleChange}
          handleBlur={handleBlur}
        />
      ),
    },
    {
      id: 2,
      title: 'ठेगाना',
      description:
        'आफ्नो सिट बुक गर्न र सहभागिता पक्का गर्न तलको फर्म भर्नुहोस्',
      component: (
        <Form2
          values={values}
          errors={errors}
          handleChange={handleChange}
          setFieldValue={setFieldValue}
          handleBlur={handleBlur}
        />
      ),
    },
    {
      id: 3,
      title: 'कृषि व्वयवसाय',
      description:
        'आफ्नो सिट बुक गर्न र सहभागिता पक्का गर्न तलको फर्म भर्नुहोस्',
      component: (
        <Form3
          values={values}
          errors={errors}
          handleChangeProfession={handleChangeProfession}
          handleChange={handleChange}
          handleBlur={handleBlur}
        />
      ),
    },
    {
      id: 4,
      title: 'कृषि व्वयवसाय',
      description: 'हाम्रा तालिमहरुको बारेमा केहि सुझाव भएमा हामीलाई दिनुहोस्',
      component: (
        <Form4
          values={values}
          errors={errors}
          handleChange={handleChange}
          handleBlur={handleBlur}
        />
      ),
    },
  ];

  const handleButtonPress = step => {
    const {
      name,
      contact_no,
      email,
      province,
      district,
      ward_no,
      body,
      profession,
    } = values;

    if (step === 0) {
      if (!name) {
        setFieldError('name', 'This field is required');
        return;
      } else if (!contact_no) {
        setFieldError('contact_no', 'This field is required');
        return;
      } else if (!email) {
        setFieldError('email', 'This field is required');
      } else {
        wizard.current.next();
      }
    } else if (step === 1) {
      if (!province) {
        setFieldError('province', 'This field is required');
        return;
      } else if (!district) {
        setFieldError('district', 'This field is required');
        return;
      } else if (!ward_no) {
        setFieldError('ward_no', 'This field is required');
      } else if (!body) {
        setFieldError('body', 'This field is required');
      } else {
        wizard.current.next();
      }
    } else if (step === 2) {
      if (!profession.length === 0) {
        setFieldError('profession', 'This field is required');
      } else {
        wizard.current.next();
      }
    } else if (step === 3) {
      handleSubmit();
    }
  };

  return (
    <View style={[t.flex1, t.bgWhite]}>
      <View style={[t.pX4, t.pY2]}>
        <Header title="सहभागिता दर्ता" />
      </View>
      <ScrollView nestedScrollEnabled={true} style={[t.bgWhite]}>
        <Loader loading={loading} />
        <View style={[t.pX4]}>
          <View style={[t.mY0]}>
            <Wizard
              ref={wizard}
              activeStep={currentStep}
              steps={steps.map(step => {
                return {
                  content: (
                    <View>
                      <Text style={[t.textLg]}>{step.title}</Text>
                      <View
                        style={[
                          t.flexRow,
                          t.itemsCenter,
                          t.justifyBetween,
                          t.flexWrap,
                        ]}>
                        <TouchableOpacity
                          onPress={() => wizard?.current?.goTo(0)}
                          style={[
                            t.h2,
                            t.w1_4,
                            t.roundedLg,
                            currentStep >= 0 ? t.bgColor1 : t.bgColor3,
                          ]}
                        />
                        <TouchableOpacity
                          onPress={() => wizard?.current?.goTo(1)}
                          style={[
                            t.h2,
                            t.w1_4,
                            t.roundedLg,
                            currentStep >= 1 ? t.bgColor1 : t.bgColor3,
                          ]}
                        />
                        <TouchableOpacity
                          onPress={() => wizard?.current?.goTo(2)}
                          style={[
                            t.h2,
                            t.w1_4,
                            t.roundedLg,
                            currentStep >= 2 ? t.bgColor1 : t.bgColor3,
                          ]}
                        />
                        <TouchableOpacity
                          onPress={() => wizard?.current?.goTo(3)}
                          style={[
                            t.h2,
                            t.w1_4,
                            t.roundedLg,
                            currentStep >= 3 ? t.bgColor1 : t.bgColor3,
                          ]}
                        />
                      </View>
                      <View style={[t.mY3]}>
                        <Text style={[t.textXl, t.fontSb, t.textText]}>
                          {step.description}
                        </Text>
                        {step.component}
                      </View>
                    </View>
                  ),
                };
              })}
              isFirstStep={val => setIsFirstStep(val)}
              isLastStep={val => setIsLastStep(val)}
              onNext={() => {
                console.log('Next Step Called');
              }}
              onPrev={() => {
                console.log('Previous Step Called');
              }}
              currentStep={({currentStep, isLastStep, isFirstStep}) => {
                setCurrentStep(currentStep);
              }}
            />
          </View>
          <Button
            onPress={() => handleButtonPress(currentStep)}
            title="अर्को कदम"
          />
        </View>
      </ScrollView>
    </View>
  );
}
