import Header from '@components/shared/Header';
import Text from '@components/shared/Text';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import {MinusSvg, PlusSvg} from '@images';
import getVideoId from 'get-video-id';
import React, {useEffect} from 'react';
import {Image, View} from 'react-native';
import Accordion from 'react-native-collapsible/Accordion';
import PTRView from 'react-native-pull-to-refresh';
import Swiper from 'react-native-swiper';
import {t} from 'react-native-tailwindcss';
import YoutubePlayer from 'react-native-youtube-iframe';

const OldTraining = ({route, navigation: {navigate}}) => {
  const {series} = route.params;
  const [upcomingTrainingsSeries, setUpcomingTrainingsSeries] = React.useState(
    [],
  );
  const [activeSections, setActiveSections] = React.useState([]);

  const getUpcomingTrainingsSeries = async () => {
    try {
      const responseTrainings = await $axios.get(
        URLS.GET_OLD_TRAININGS_SERIES_URL(series?.series),
      );
      setUpcomingTrainingsSeries(responseTrainings.data);
    } catch (error) {}
  };

  useEffect(() => {
    getUpcomingTrainingsSeries();
  }, []);

  const handleRefresh = async () => {
    await getUpcomingTrainingsSeries();
  };

  const _renderHeader = (section, index, isActive) => {
    return (
      <View
        style={[t.pY2, t.pX3, t.flex, t.flexRow, t.justifyBetween, t.flexWrap]}>
        <View style={[t.flex, t.flexRow, t.flexWrap, t.w11_12]}>
          <Text style={[t.textLg, t.textMediumBlack, t.fontB]}>
            {section.title}
          </Text>
        </View>
        <View>{isActive ? <MinusSvg /> : <PlusSvg />}</View>
      </View>
    );
  };

  const _renderContent = section => {
    const sliders = section?.resources?.filter(
      item => item.file_type !== 'video',
    );
    return (
      <View style={[t.bgBg, t.p4]}>
        <View style={[]}>
          <Image
            resizeMode="stretch"
            style={[t.wFull, t.h56, t.roundedSm, t.mB2]}
            source={{
              uri: section.thumbnail,
            }}
          />
          <Text style={[t.textBase, t.textText]}>{section.description}</Text>
          {sliders?.length > 0 && (
            <Swiper
              style={[t.h56]}
              buttonWrapperStyle={[t.textWhite]}
              showsButtons
              showsPagination={false}
              loop={false}>
              {sliders?.map((item, index) => (
                <View style={[t.mY2]} key={index}>
                  <Image
                    style={[t.wFull, t.h56, t.roundedLg, t.mB2]}
                    source={{
                      uri: item.file,
                    }}
                  />
                </View>
              ))}
            </Swiper>
          )}

          {section?.resources
            ?.filter(item => item.file_type === 'video')
            .map((resource, index) => {
              let videoId =
                resource?.file_type === 'video'
                  ? getVideoId(resource?.link || 'SyjAhqU0Wp8')?.id
                  : 'SyjAhqU0Wp8';
              return (
                <View style={[t.mY2]} key={index}>
                  <YoutubePlayer
                    webViewStyle={{opacity: 0.99}}
                    height={224}
                    play={false}
                    videoId={videoId}
                  />
                </View>
              );
            })}
        </View>
      </View>
    );
  };

  const _updateSections = data => {
    setActiveSections(data);
  };

  return (
    <View style={[t.flex1, t.bgWhite]}>
      <View style={[t.pX4, t.pY2]}>
        <Header title="पुरानो तालिम श्रृंखला" />
      </View>
      <PTRView onRefresh={handleRefresh}>
        <Accordion
          underlayColor="#fff"
          sections={upcomingTrainingsSeries}
          activeSections={activeSections}
          renderHeader={_renderHeader}
          renderContent={_renderContent}
          onChange={_updateSections}
        />
      </PTRView>
    </View>
  );
};
export default OldTraining;
