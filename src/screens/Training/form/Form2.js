import TextInput from '@components/shared/TextInput';
import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import Text from '@components/shared/Text';
import collections from '../../../data/collections.json';
import DropDownPicker from 'react-native-dropdown-picker';
import {capitalize, uniqBy} from 'lodash';

const Form2 = ({handleChange, handleBlur, values, errors, setFieldValue}) => {
  const provinces = uniqBy(collections, 'id').map(item => ({
    label: item.id,
    value: item.id,
  }));

  const districts = uniqBy(collections, 'district')
    .filter(item => item.id == values.province)
    .map(item => ({
      label: capitalize(item.district),
      value: capitalize(item.district),
    }));

  const localBodies = uniqBy(collections, 'localBody')
    .filter(item => capitalize(item.district) === values.district)
    .map(item => ({
      label: item.localBody,
      value: item.localBody,
    }));

  const [open1, setOpen1] = useState(false);
  const [items1, setItems1] = useState(provinces);

  const [open2, setOpen2] = useState(false);
  const [items2, setItems2] = useState(districts);

  const [open3, setOpen3] = useState(false);
  const [items3, setItems3] = useState(localBodies);

  useEffect(() => {
    setItems1(provinces);
    setItems2(districts);
    setItems3(localBodies);
  }, [values.province, values.district]);

  return (
    <View>
      <View style={[t.mY3]}>
        <Text style={[t.textLg, t.mL4, t.fontSb, t.textColor2]}>
          तपाईको प्रदेश
        </Text>
        <DropDownPicker
          placeholder="तपाईको प्रदेश"
          searchable
          open={open1}
          value={values.province}
          listMode="MODAL"
          items={items1}
          labelStyle={[t.textLg, t.fontR, t.textText]}
          placeholderStyle={[t.fontR, t.textBase]}
          listItemLabelStyle={[t.fontR, t.textLg, t.textText]}
          setOpen={setOpen1}
          setValue={state => {
            let newState = state;

            if (typeof state === 'function') {
              newState = state(values.priority);
            }
            setFieldValue('province', newState);
          }}
          setItems={setItems1}
          style={
            errors.province
              ? [t.borderColor6, t.bgBgColor2]
              : [t.textBase, t.borderBorder]
          }
          searchContainerStyle={t.borderBorder}
        />
      </View>
      <View style={[t.mY3]}>
        <Text style={[t.textLg, t.mL4, t.fontSb, t.textColor2]}>
          तपाईको जिल्ला
        </Text>
        <DropDownPicker
          placeholder="तपाईको जिल्ला"
          searchable
          open={open2}
          value={values.district}
          items={items2}
          listMode="MODAL"
          labelStyle={[t.textLg, t.fontR, t.textText]}
          placeholderStyle={[t.fontR, t.textBase, t.textColor2]}
          listItemLabelStyle={[t.fontR, t.textLg, t.textText]}
          setOpen={setOpen2}
          setValue={state => {
            let newState = state;
            if (typeof state === 'function') {
              newState = state(values.priority);
            }
            setFieldValue('district', newState);
          }}
          setItems={setItems2}
          style={
            errors.district
              ? [t.borderColor6, t.bgBgColor2]
              : [t.textBase, t.borderBorder]
          }
          searchContainerStyle={t.borderBorder}
        />
      </View>
      <View style={[t.mY3]}>
        <Text style={[t.textLg, t.mL4, t.fontSb, t.textColor2]}>
          तपाईको निकाय
        </Text>
        <DropDownPicker
          placeholder="तपाईको निकाय"
          searchable
          open={open3}
          value={values.body}
          items={items3}
          listMode="MODAL"
          labelStyle={[t.textLg, t.fontR, t.textText]}
          placeholderStyle={[t.fontR, t.textBase, t.textColor2]}
          listItemLabelStyle={[t.fontR, t.textLg, t.textText]}
          setOpen={setOpen3}
          setValue={state => {
            let newState = state;
            if (typeof state === 'function') {
              newState = state(values.priority);
            }
            setFieldValue('body', newState);
          }}
          setItems={setItems3}
          style={
            errors.body
              ? [t.borderColor6, t.bgBgColor2]
              : [t.textBase, t.borderBorder]
          }
          searchContainerStyle={t.borderBorder}
        />
      </View>
      <View style={[t.mY3]}>
        <Text style={[t.textLg, t.mL4, t.fontSb, t.textColor2]}>
          तपाईको वडा
        </Text>
        <TextInput
          keyboardType="numeric"
          onChangeText={handleChange('ward_no')}
          onBlur={handleBlur('ward_no')}
          value={values.ward_no}
          placeholder="तपाईको वडा"
          errorMessage={errors.ward_no}
          style={[t.roundedLg, t.fontSb, t.mY2, t.textLg, t.textColor2]}
        />
      </View>
    </View>
  );
};

export default Form2;
