import TextInput from '@components/shared/TextInput';
import React from 'react';
import {View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import Text from '@components/shared/Text';
import CheckBox from '@react-native-community/checkbox';
import isEmpty from 'lodash/isEmpty';

const data = [
  {
    id: 1,
    label: 'व्यवसायिक तरकारी तथा फलफुल खेती',
    value: 'व्यवसायिक तरकारी तथा फलफुल खेती',
  },
  {id: 2, label: 'धान खेती तथा अन्नबाली', value: 'धान खेती तथा अन्नबाली'},
  {id: 3, label: 'अन्यव्यवसायिक माछापालन', value: 'व्यवसायिक माछापालन'},
  {id: 4, label: 'कुखुरा वा बङ्गुरपालन', value: 'कुखुरा वा बङ्गुरपालन'},
  {id: 5, label: 'कृषिको विद्यार्थी ', value: 'कृषिको विद्यार्थी'},
  {
    id: 6,
    label: 'अन्य (कृपया व्यवसाय खुलाउनुहोला)',
    value: 'अन्य (कृपया व्यवसाय खुलाउनुहोला)',
  },
];

const Form3 = ({
  handleChangeProfession,
  values,
  errors,
  handleChange,
  handleBlur,
}) => {
  return (
    <View>
      <View style={[t.mY3]}>
        <Text style={[t.textLg, t.mL4, t.fontSb, t.textColor2]}>
          तपाईको कृषि व्यवसायहरु चयन गर्नुहोस्
        </Text>
        {data.map(d => (
          <View key={d.id} style={[t.flexRow, t.mY1, t.itemsCenter]}>
            <CheckBox
              tintColors={{
                true: '#469FD8',
                false: '#EDEDED',
              }}
              value={values.profession?.includes(d.value)}
              onValueChange={newValue =>
                handleChangeProfession(newValue, d.value)
              }
            />
            <Text style={[t.textLg, t.textText]}>{d.label}</Text>
          </View>
        ))}
        {values?.profession?.includes('अन्य (कृपया व्यवसाय खुलाउनुहोला)') && (
          <>
            <TextInput
              onChangeText={handleChange('other')}
              onBlur={handleBlur('other')}
              value={values.other}
              placeholder="व्यवसायको नाम"
              style={[t.roundedLg, t.fontSb, t.mY2, t.textLg, t.textColor2]}
              errorMessage={errors.name}
            />
          </>
        )}
        {!isEmpty(errors) && (
          <View
            style={[
              t.flex,
              t.justifyCenter,
              t.itemsCenter,
              t.flexCol,
              t.mY2,
              t.bgBgColor2,
              t.p3,
              t.roundedLg,
            ]}>
            {Object.entries(errors).map(key => {
              return (
                <Text style={[t.textColor6, t.textBase, t.fontSb]}>
                  {key[1]}
                </Text>
              );
            })}
          </View>
        )}
      </View>
    </View>
  );
};

export default Form3;
