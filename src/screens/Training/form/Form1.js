import TextInput from '@components/shared/TextInput';
import React from 'react';
import {View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import Text from '@components/shared/Text';

const Form1 = ({handleChange, handleBlur, values, errors}) => {
  return (
    <View>
      <View style={[t.mY3]}>
        <Text style={[t.textLg, t.mL4, t.fontSb, t.textColor2]}>नाम</Text>
        <TextInput
          onChangeText={handleChange('name')}
          onBlur={handleBlur('name')}
          value={values.name}
          placeholder="तपाईको नाम"
          style={[t.roundedLg, t.fontSb, t.mY2, t.textLg, t.textColor2]}
          errorMessage={errors.name}
        />
      </View>
      <View style={[t.mY3]}>
        <Text style={[t.textLg, t.mL4, t.fontSb, t.textColor2]}>फोन नम्बर</Text>
        <TextInput
          onChangeText={handleChange('contact_no')}
          onBlur={handleBlur('contact_no')}
          keyboardType="numeric"
          value={values.contact_no}
          placeholder="तपाईको फोन नम्बर"
          style={[t.roundedLg, t.fontSb, t.mY2, t.textLg, t.textColor2]}
          errorMessage={errors.contact_no}
        />
      </View>
      <View style={[t.mY3]}>
        <Text style={[t.textLg, t.mL4, t.fontSb, t.textColor2]}>
          तपाईको इमेल
        </Text>
        <TextInput
          onChangeText={handleChange('email')}
          onBlur={handleBlur('email')}
          value={values.email}
          placeholder="तपाईको इमेल"
          style={[t.roundedLg, t.fontSb, t.mY2, t.textLg, t.textColor2]}
          errorMessage={errors.email}
        />
      </View>
    </View>
  );
};

export default Form1;
