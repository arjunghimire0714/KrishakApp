import TextInput from '@components/shared/TextInput';
import React from 'react';
import {View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import Text from '@components/shared/Text';
import isEmpty from 'lodash/isEmpty';

const Form4 = ({handleChange, handleBlur, values, errors}) => {
  return (
    <View style={[t.mY3]}>
      <TextInput
        multiline={true}
        numberOfLines={14}
        onChangeText={handleChange('response')}
        onBlur={handleBlur('response')}
        value={values.response}
        style={[
          t.roundedLg,
          {textAlignVertical: 'top'},
          t.fontSb,
          t.mY2,
          t.textLg,
          t.textColor2,
        ]}
        errorMessage={errors.response}
      />
      {!isEmpty(errors) && (
        <View
          style={[
            t.flex,
            t.justifyCenter,
            t.itemsCenter,
            t.flexCol,
            t.mY2,
            t.bgBgColor2,
            t.p3,
            t.roundedLg,
          ]}>
          {Object.entries(errors).map(key => {
            return (
              <Text style={[t.textColor6, t.textBase, t.fontSb]}>{key[1]}</Text>
            );
          })}
        </View>
      )}
    </View>
  );
};

export default Form4;
