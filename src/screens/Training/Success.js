import Button from '@components/shared/Button';
import Text from '@components/shared/Text';
import {TrainingSuccessIcon} from '@images';
import React from 'react';
import {ScrollView, View} from 'react-native';
import {t} from 'react-native-tailwindcss';

const Success = ({navigation: {navigate}}) => {
  return (
    <ScrollView contentContainerStyle={[t.flexGrow]} style={[t.bgColor1, t.p4]}>
      <View
        style={[
          t.bgColor1,
          t.flex1,
          t.itemsCenter,
          t.justifyBetween,
          t.flexWrap,
          t.p4,
        ]}>
        <View style={[t.bgColor1, t.flex, t.itemsCenter, t.justifyBetween]}>
          <Text style={[t.text3xl, t.fontB, t.textCenter, t.textWhite]}>
            तपाईको सहभागिताको दर्ता
          </Text>
          <Text style={[t.text3xl, t.fontB, t.textCenter, t.textWhite]}>
            सफल भएको छ ।
          </Text>
        </View>
        <View style={[t.bgColor5, t.wFull, t.itemsCenter, t.roundedLg, t.p6]}>
          <TrainingSuccessIcon />
        </View>
        <Button
          btnStyle={[t.bgWhite, t.wFull]}
          textStyle={[t.textColor1]}
          onPress={() => navigate('Home')}
          title="फिर्ता जानुहोस्"
        />
      </View>
    </ScrollView>
  );
};

export default Success;
