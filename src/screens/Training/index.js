import Button from '@components/shared/Button';
import Header from '@components/shared/Header';
import Text from '@components/shared/Text';
import Timer from '@components/shared/Timer';
import Zoom from '@components/shared/Zoom';
import {moment, times} from '@helpers';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import {useIsFocused} from '@react-navigation/native';
import {englishToNepaliNumber} from 'nepali-number';
import React, {useEffect} from 'react';
import {Image, TouchableOpacity, View} from 'react-native';
import PTRView from 'react-native-pull-to-refresh';
import {t} from 'react-native-tailwindcss';
import {useSelector} from 'react-redux';
import Loader from '@components/shared/Loader';

const adbs = require('ad-bs-converter');

const Training = ({navigation: {navigate}}) => {
  const isFocused = useIsFocused();
  const {userDetails} = useSelector(state => state.auth);
  const [ongoingTrainings, setOngoingTrainings] = React.useState([]);
  const [upcomingTrainings, setUpcomingTrainings] = React.useState([]);
  const [oldTrainings, setOldTrainings] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [next1, setNext1] = React.useState(false);
  const [page1, setPage1] = React.useState(1);

  const [next2, setNext2] = React.useState(false);
  const [page2, setPage2] = React.useState(1);

  const [next3, setNext3] = React.useState(false);
  const [page3, setPage3] = React.useState(1);

  const getOngoingTrainings = async (searchQuery, nextStep = false) => {
    try {
      const responseTrainings = await $axios.get(
        URLS.GET_ONGOING_TRAININGS_URL(searchQuery),
      );
      if (nextStep) {
        setPage3(page3 + 1);
        setNext3(responseTrainings.data.next ? true : false);
        setOngoingTrainings([
          ...ongoingTrainings,
          ...responseTrainings.data.results,
        ]);
      } else {
        setNext3(responseTrainings.data.next ? true : false);
        setOngoingTrainings(responseTrainings.data.results);
      }
    } catch (error) {
      console.log('ERROR ONGOING', error);
    }
  };

  const getUpcomingTrainings = async (searchQuery, nextStep = false) => {
    try {
      setLoading(true);
      const responseTrainings = await $axios.get(
        URLS.GET_UPCOMING_TRAININGS_URL(searchQuery),
      );
      setLoading(false);
      if (nextStep) {
        setPage1(page1 + 1);
        setNext1(responseTrainings.data.next ? true : false);
        setUpcomingTrainings([
          ...upcomingTrainings,
          ...responseTrainings.data.results,
        ]);
      } else {
        setNext1(responseTrainings.data.next ? true : false);
        setUpcomingTrainings(responseTrainings.data.results);
      }
    } catch (error) {
      console.log('ERROR UPCOMMING', error);
      setLoading(false);
    }
  };

  const getOldTrainings = async (searchQuery, nextStep = false) => {
    try {
      const responseTrainings = await $axios.get(
        URLS.GET_OLD_TRAININGS_URL(searchQuery),
      );
      if (nextStep) {
        setPage2(page2 + 1);
        setNext2(responseTrainings.data.next ? true : false);
        setOldTrainings([...oldTrainings, ...responseTrainings.data.results]);
      } else {
        setNext2(responseTrainings.data.next ? true : false);
        setOldTrainings(responseTrainings.data.results);
      }
    } catch (error) {
      console.log('ERROR OLD', error);
    }
  };

  useEffect(() => {
    getOngoingTrainings();
    getUpcomingTrainings();
    getOldTrainings();
  }, [isFocused]);

  const handleRefresh = async () => {
    await getOngoingTrainings();
    await getUpcomingTrainings();
    await getOldTrainings();
  };

  return (
    <View style={[t.flex1, t.bgWhite]}>
      <View style={[t.pX4, t.pY2]}>
        <Header title="सहभागिता दर्ता" />
      </View>
      <Loader loading={loading} />
      <PTRView onRefresh={handleRefresh}>
        <View>
          {ongoingTrainings?.length > 0 && (
            <View style={[t.p4, t.mT3]}>
              <Text style={[t.textXl, t.textText, t.fontSb]}>
                हाम्रो चलिरहेका कृषि तालिम
              </Text>
            </View>
          )}
          {ongoingTrainings?.length > 0 &&
            ongoingTrainings
              ?.filter(item => item.has_registered)
              .map(item => {
                const momentObj = moment(
                  item.training_date + ' ' + item.training_time,
                  'YYYY-MM-DD hh:mm:ss',
                );
                const {
                  ne: {strMonth, day, strDayOfWeek},
                } = adbs.ad2bs(moment(item.training_date).format('YYYY/MM/DD'));
                return (
                  <View key={item.id} style={[t.mY1, t.bgBorder, t.p4]}>
                    <Image
                      style={[t.wFull, t.h48, t.roundedLg, t.mB2]}
                      source={{
                        uri: item.thumbnail,
                      }}
                    />
                    <Text style={[t.text2xl, t.fontB, t.textBlack]}>
                      {item.title}
                    </Text>
                    <Text style={[t.textXl, t.fontSb, t.textText]}>
                      {item.description}
                    </Text>
                    <View style={[t.mY3]}>
                      <Text style={[t.textLg, t.fontR, t.textBlack]}>
                        मिति : {`${strMonth} ${day}, ${strDayOfWeek}`}
                      </Text>
                      <Text style={[t.textLg, t.fontR, t.textBlack]}>
                        समय : {times[momentObj.format('a')]}{' '}
                        {englishToNepaliNumber(momentObj.format('hh'))} :
                        {englishToNepaliNumber(momentObj.format('mm'))} बजे
                      </Text>
                    </View>
                    <Zoom
                      id={userDetails?.id}
                      name={userDetails?.name || 'Bot'}
                      zoomLink={item?.zoom_link}
                    />
                  </View>
                );
              })}
          {next3 && (
            <View style={[t.mX3]}>
              <Button
                onPress={() =>
                  getUpcomingTrainings(`?page=${page3 + 1}`, next3)
                }
                title="सबै चलिरहेका तालिम श्रृंखलाहरु"
              />
            </View>
          )}
          <View style={[t.p4, t.mT3]}>
            <Text style={[t.textXl, t.textText, t.fontSb]}>
              हाम्रो आगामि कृषि तालिम
            </Text>
          </View>
          {upcomingTrainings?.length > 0 &&
            upcomingTrainings?.map(item => {
              const momentObj = moment(
                item.training_date + ' ' + item.training_time,
                'YYYY-MM-DD hh:mm:ss',
              );
              const {
                ne: {strMonth, day, strDayOfWeek},
              } = adbs.ad2bs(moment(item.training_date).format('YYYY/MM/DD'));
              const momentCurrentObj = moment().format('YYYY-MM-DD hh:mm:ss a');
              const startDate = moment(momentObj, 'YYYY-MM-DD hh:mm:ss a');
              const endDate = moment(momentCurrentObj, 'YYYY-MM-DD hh:mm:ss a');
              const diff = startDate.diff(endDate, 'seconds');
              return (
                <View key={item.id} style={[t.mY1, t.bgBorder, t.p4]}>
                  <Image
                    style={[t.wFull, t.h48, t.roundedLg, t.mB2]}
                    source={{
                      uri: item.thumbnail,
                    }}
                  />
                  <Text style={[t.text2xl, t.fontB, t.textBlack]}>
                    {item.title}
                  </Text>
                  <Text style={[t.textXl, t.fontSb, t.textText]}>
                    {item.description}
                  </Text>
                  <View style={[t.mY3]}>
                    <Text style={[t.textLg, t.fontR, t.textBlack]}>
                      मिति : {`${strMonth} ${day}, ${strDayOfWeek}`}
                    </Text>
                    <Text style={[t.textLg, t.fontR, t.textBlack]}>
                      समय : {times[momentObj.format('a')]}{' '}
                      {englishToNepaliNumber(momentObj.format('hh'))} :
                      {englishToNepaliNumber(momentObj.format('mm'))} बजे
                    </Text>
                  </View>
                  {item.has_registered && diff > 0 && (
                    <Button
                      disabled
                      title={
                        <Timer
                          primary
                          duration={diff}
                          handleFinished={handleRefresh}
                        />
                      }
                    />
                  )}
                  {diff < 0 && item?.has_registered && (
                    <Zoom
                      id={userDetails?.id}
                      name={userDetails?.name || 'Bot'}
                      zoomLink={item?.zoom_link}
                    />
                  )}
                  {!item.has_registered && (
                    <Button
                      onPress={() =>
                        navigate('Participation', {
                          trainingId: item.id,
                        })
                      }
                      title="आफ्नो सिट बुक गर्नुहोस्"
                    />
                  )}
                </View>
              );
            })}
          {next1 && (
            <View style={[t.mX3]}>
              <Button
                onPress={() =>
                  getUpcomingTrainings(`?page=${page1 + 1}`, next1)
                }
                title="सबै आगामि कृषि तालिम श्रृंखलाहरु"
              />
            </View>
          )}
          <View style={[t.p4]}>
            <Text style={[t.textLg, t.fontSb, t.mY4, t.textText]}>
              हाम्रा पुराना कृषि तालिम श्रृंखलाहरु
            </Text>
            {oldTrainings?.length > 0 &&
              oldTrainings.map(item => (
                <TouchableOpacity
                  key={item.id}
                  onPress={() =>
                    navigate('OldTraining', {
                      series: item,
                    })
                  }
                  style={[t.mY2]}>
                  <Image
                    style={[t.wFull, t.roundedLg, t.mB2]}
                    source={{
                      uri: item.thumbnail,
                    }}
                  />
                  <Text style={[t.text2xl, t.fontSb, t.textMediumBlack]}>
                    {item.series}
                  </Text>
                  <View style={[t.flex, t.flexRow, t.mY3, t.justifyBetween]}>
                    <View style={[t.flex, t.flexRow]}>
                      <Text style={[t.textBase, t.fontSb, t.textColor2]}>
                        {englishToNepaliNumber(item.no_of_traings)} तालिमहरु
                      </Text>
                    </View>
                    <View style={[t.flex, t.flexRow]}>
                      <Text style={[t.textBase, t.fontSb, t.textColor2]}>
                        {item.no_or_participants === 0
                          ? `${englishToNepaliNumber(
                              item.no_or_participants,
                            )} सहभागि`
                          : `${englishToNepaliNumber(
                              item.no_or_participants,
                            )} सहभागिहरु`}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              ))}
            {next2 && (
              <View style={[t.mX3]}>
                <Button
                  onPress={() => getOldTrainings(`?page=${page2 + 1}`, next2)}
                  title="सबै पुराना कृषि तालिम श्रृंखलाहरु"
                />
              </View>
            )}
          </View>
        </View>
      </PTRView>
    </View>
  );
};
export default Training;
