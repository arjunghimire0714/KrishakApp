import Button from '@components/shared/Button';
import Header from '@components/shared/Header';
import Loader from '@components/shared/Loader';
import Text from '@components/shared/Text';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import MasonryList from '@react-native-seoul/masonry-list';
import {englishToNepaliNumber} from 'nepali-number';
import React, {useEffect} from 'react';
import {Image, ScrollView, StyleSheet, View} from 'react-native';
import {Gravatar} from 'react-native-gravatar';
import {t} from 'react-native-tailwindcss';
import {useSelector} from 'react-redux';

const Winner = ({route}) => {
  const [leaders, setLeaders] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [next, setNext] = React.useState(false);
  const [page, setPage] = React.useState(1);
  const {userDetails} = useSelector(state => state.auth);
  const {quizId} = route.params;

  const getLeaderBoard = async (searchQuery, nextStep = false) => {
    try {
      setLoading(true);
      const responseLeaders = await $axios.get(
        URLS.GET_QUIZ_LEADERBOARD_URL(quizId, searchQuery),
      );
      setLoading(false);
      if (nextStep) {
        setPage(page + 1);
        setNext(responseLeaders.data.next ? true : false);
        setLeaders([...leaders, ...responseLeaders.data.results]);
      } else {
        setNext(responseLeaders.data.next ? true : false);
        setLeaders(responseLeaders.data.results);
      }
    } catch (error) {
      setLoading(true);
    }
  };

  useEffect(() => {
    getLeaderBoard();
  }, []);
  return (
    <View style={[t.flex1]}>
      <View style={[t.pX4, t.pY2]}>
        <Header title="विजेताहरु" />
      </View>
      <Loader loading={loading} />
      <ScrollView
        contentContainerStyle={[t.flexGrow]}
        style={[t.bgWhite, t.flex1]}>
        <MasonryList
          style={t.selfStretch}
          contentContainerStyle={[t.pX5, t.selfStretch]}
          numColumns={1}
          data={leaders}
          keyExtractor={(_, index) => index.toString()}
          renderItem={({item, i}) => (
            <View key={item.id}>
              <View
                style={[
                  t.border2,
                  t.p4,
                  t.roundedFull,
                  t.mY2,
                  t.relative,
                  t.itemsCenter,
                  userDetails?.email === item?.email ||
                  userDetails?.id === item?.user_id
                    ? t.bgBgColor1
                    : t.bgColor3,
                  userDetails?.email === item?.email ||
                  userDetails?.id === item?.user_id
                    ? t.borderColor1
                    : t.borderBorder,
                ]}>
                <Text style={[t.textMediumBlack, t.textBase, t.fontSb]}>
                  {item.name}
                </Text>
                <View
                  style={[
                    t.absolute,
                    t.border,
                    styles.card,
                    t.w12,
                    t.bgWhite,
                    t.h12,
                    t.justifyCenter,
                    t.itemsCenter,
                    t.border2,
                    t.roundedFull,
                    t.borderBorder,
                  ]}>
                  <Image
                    style={[t.w12, t.h12, t.roundedFull]}
                    source={
                      item?.profile_pic || item?.profile_pic_url
                        ? {
                            uri: item?.profile_pic_url || item?.profile_pic,
                          }
                        : require('@images/logo.png')
                    }
                  />
                </View>
                <View
                  style={[
                    t.absolute,
                    styles.card1,
                    t.w12,
                    t.h12,
                    t.justifyCenter,
                    t.itemsCenter,
                    t.roundedFull,
                    t.borderBorder,
                  ]}>
                  <Text style={[t.text2xl, t.fontB, t.textColor2]}>
                    {englishToNepaliNumber(item.rank)}
                  </Text>
                </View>
              </View>
            </View>
          )}
        />
        {next && (
          <View style={[t.mX3, t.mB3]}>
            <Button
              onPress={() => getLeaderBoard(`?page=${page + 1}`, next)}
              title="सबै विजेताहरु हेर्नुहोस"
            />
          </View>
        )}
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    top: 3,
    left: 5,
  },
  card1: {
    top: 5,
    right: 5,
  },
});
export default Winner;
