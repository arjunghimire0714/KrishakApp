import Button from '@components/shared/Button';
import Text from '@components/shared/Text';
import {TimerIcon} from '@images';
import React from 'react';
import {ScrollView, View} from 'react-native';
import {t} from 'react-native-tailwindcss';

const Timer = ({route, navigation: {navigate}}) => {
  const {quizId} = route.params;

  return (
    <ScrollView contentContainerStyle={[t.flexGrow]} style={[t.bgColor1, t.p4]}>
      <View
        style={[
          t.bgColor1,
          t.flex1,
          t.itemsCenter,
          t.justifyBetween,
          t.flexWrap,
        ]}>
        <View style={[t.bgColor1, t.flex, t.itemsCenter, t.justifyBetween]}>
          <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
            माफ गर्नुहोस् !
          </Text>
          <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
            तपाईको समय समाप्त भएको छ
          </Text>
        </View>
        <View style={[t.bgColor5, t.wFull, t.itemsCenter, t.roundedLg, t.p6]}>
          <TimerIcon />
        </View>
        <View style={[t.flexCol, t.wFull]}>
          <Button
            btnStyle={[t.bgWhite, t.wFull, t.mB2]}
            textStyle={[t.textColor1]}
            onPress={() =>
              navigate('Winner', {
                quizId,
              })
            }
            title="लिडरबोर्डमा जानुहोस्"
          />
          <Button
            btnStyle={[t.bgWhite, t.wFull]}
            textStyle={[t.textColor1]}
            onPress={() => navigate('Main')}
            title="फिर्ता जानुहोस्"
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default Timer;
