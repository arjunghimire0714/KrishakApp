import Header from '@components/shared/Header';
import Loader from '@components/shared/Loader';
import Text from '@components/shared/Text';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import {englishToNepaliNumber} from 'nepali-number';
import React, {useEffect} from 'react';
import {FlatList, Image, StyleSheet, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {useSelector} from 'react-redux';

const Weekly = () => {
  const [leaders, setLeaders] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const {userDetails} = useSelector(state => state.auth);

  const getLeaderBoard = async (nextStep = false) => {
    try {
      setLoading(true);
      const responseLeaders = await $axios.get(
        URLS.GET_QUIZ_WEEKLY_LEADERBOARD_URL,
      );
      setLeaders(responseLeaders?.data?.leaderboard);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getLeaderBoard();
  }, []);

  return (
    <View style={[t.flex1]}>
      <View style={[t.pX4, t.pY2]}>
        <Header title="साप्ताहिक लिडरबोर्ड" />
      </View>
      <Loader loading={loading} />
      <FlatList
        contentContainerStyle={[t.pX5, t.selfStretch]}
        data={leaders}
        initialNumToRender={10}
        keyExtractor={(_, index) => index.toString()}
        renderItem={({item, i}) => (
          <View key={item.id}>
            <View
              style={[
                t.border2,
                t.p4,
                t.roundedFull,
                t.mY2,
                t.relative,
                t.itemsCenter,
                userDetails?.email === item?.email ||
                userDetails?.id === item?.user_id
                  ? t.bgBgColor1
                  : t.bgColor3,
                userDetails?.email === item?.email ||
                userDetails?.id === item?.user_id
                  ? t.borderColor1
                  : t.borderBorder,
              ]}>
              <Text style={[t.textMediumBlack, t.textBase, t.fontSb]}>
                {item.name}
              </Text>
              <View
                style={[
                  t.absolute,
                  t.border,
                  styles.card,
                  t.w12,
                  t.bgWhite,
                  t.h12,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.border2,
                  t.roundedFull,
                  t.borderBorder,
                ]}>
                <Image
                  style={[t.w12, t.h12, t.roundedFull]}
                  source={
                    item?.profile_pic || item?.profile_pic_url
                      ? {
                          uri: item?.profile_pic_url || item?.profile_pic,
                        }
                      : require('@images/logo.png')
                  }
                />
              </View>
              <View
                style={[
                  t.absolute,
                  styles.card1,
                  t.w12,
                  t.h12,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.roundedFull,
                  t.borderBorder,
                ]}>
                <Text style={[t.text2xl, t.fontB, t.textColor2]}>
                  {englishToNepaliNumber(item.rank)}
                </Text>
              </View>
            </View>
          </View>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    top: 3,
    left: 5,
  },
  card1: {
    top: 5,
    right: 5,
  },
});
export default Weekly;
