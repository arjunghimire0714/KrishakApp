/* eslint-disable react-hooks/exhaustive-deps */
import Text from '@components/shared/Text';
import Timer from '@components/shared/Timer1';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import React, {useEffect, useState} from 'react';
import {ScrollView, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import PP from './format/PP';
import PT from './format/PT';
import TP from './format/TP';
import TT from './format/TT';
import Button from '@components/shared/Button';
import Loader from '@components/shared/Loader';
import {useIsFocused} from '@react-navigation/native';

const Contests = ({navigation: {navigate}}) => {
  const isFocused = useIsFocused();

  const [loading, setLoading] = useState(false);
  const [quizzes, setQuizzes] = React.useState([]);
  const [showTimer, setShowTimer] = React.useState(false);
  const [checkAnswer, setCheckAnswer] = React.useState({
    correct: false,
    index: 0,
    selected: false,
  });
  const [timer, setTimer] = useState(1);

  const getQuizzes = async () => {
    try {
      setLoading(true);
      const responseSuggestions = await $axios.get(URLS.GET_QUIZZES);
      setQuizzes(responseSuggestions.data.results);
      setShowTimer(true);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const handleBack = async () => {
    setShowTimer(false);
    const quizAnswer = quizzes?.slice(0, 1)?.[0];
    const findWrongAnswer = quizAnswer?.quiz_answers?.find(
      a => a.is_correct === false,
    );
    const sendData = {
      answer_id: findWrongAnswer?.id,
      time: timer,
    };

    const quizId = quizzes?.slice(0, 1)?.[0]?.id;
    try {
      await $axios.post(URLS.SUBMIT_QUIZ_URL(quizId), sendData);
    } catch (error) {}
  };

  const handleFinished = async () => {
    setShowTimer(false);
    const quizAnswer = quizzes?.slice(0, 1)?.[0];
    const findWrongAnswer = quizAnswer?.quiz_answers?.find(
      a => a.is_correct === false,
    );
    const sendData = {
      answer_id: findWrongAnswer?.id,
      time: timer,
    };
    const quizId = quizzes?.slice(0, 1)?.[0]?.id;
    try {
      await $axios.post(URLS.SUBMIT_QUIZ_URL(quizId), sendData);
      navigate('Timer', {
        quizId,
      });
    } catch (error) {
      navigate('Timer', {
        quizId,
      });
    }
  };

  const handleQuizSubmit = async (data, answer, index, answerId) => {
    setShowTimer(false);
    setCheckAnswer({
      correct: answer,
      index,
      selected: true,
    });
    const sendData = {
      answer_id: answerId,
      time: timer,
    };
    setTimeout(async () => {
      const quizId = quizzes?.slice(0, 1)?.[0]?.id;
      try {
        await $axios.post(URLS.SUBMIT_QUIZ_URL(quizId), sendData);
      } catch (error) {}
      if (answer) {
        navigate('Correct', {
          quizId,
        });
      } else {
        navigate('InCorrect');
      }
    }, 2000);
  };

  useEffect(() => {
    getQuizzes();
  }, [isFocused]);

  useEffect(() => {
    return () => {
      if (quizzes?.length > 0) {
        const has_participated = quizzes?.slice(0, 1)?.[0]?.has_participated;
        if (!has_participated) {
          handleBack();
        }
      }
    };
  }, [quizzes]);

  useEffect(() => {
    let timerId;
    if (quizzes?.length > 0) {
      timerId = setInterval(() => setTimer(timer + 1), 1000);
    }
    return () => clearInterval(timerId);
  });

  return (
    <ScrollView
      contentContainerStyle={[t.flexGrow]}
      style={[t.bgWhite, t.flex1]}>
      <Loader loading={loading} />
      <View style={[t.bgWhite, t.flex1, t.justifyBetween]}>
        <View style={[t.flex1]}>
          {quizzes?.slice(0, 1).map((quiz, index) => {
            if (quiz.has_participated) {
              return (
                <View style={[t.flex1, t.justifyBetween, t.p4, t.mY4]}>
                  <View>
                    <Text
                      style={[t.fontSb, t.textText, t.textCenter, t.text2xl]}>
                      आजको कृषि प्रतियोगितामा सहभागी हुनुभएकोमा धन्यवाद!
                    </Text>
                    <Text
                      style={[t.fontSb, t.textText, t.textCenter, t.text2xl]}>
                      कृपया अर्को प्रतियोगिताको लागि भोलि पुन: जोडिनुहोला!
                    </Text>
                  </View>
                  <View>
                    <View style={[t.mY2]}>
                      <Button
                        onPress={() =>
                          navigate('Winner', {
                            quizId: quiz.id,
                          })
                        }
                        title="लिडरबोर्डमा जानुहोस्"
                      />
                    </View>
                    <View style={[t.mY2]}>
                      <Button
                        style={[t.mY2]}
                        onPress={() => navigate('Main')}
                        title="फिर्ता जानुहोस्"
                      />
                    </View>
                  </View>
                </View>
              );
            }
            switch (quiz.question_format) {
              case 'pp':
                return (
                  <View>
                    <PP
                      handleFinished={handleFinished}
                      key={index}
                      quiz={quiz}
                      checkAnswer={checkAnswer}
                      handleQuizSubmit={handleQuizSubmit}
                    />
                    {showTimer && (
                      <View
                        style={[
                          t.justifyCenter,
                          t.mY4,
                          t.itemsCenter,
                          t.flexCol,
                        ]}>
                        <Timer
                          disableHoursLimit
                          duration={40}
                          handleFinished={handleFinished}
                        />
                      </View>
                    )}
                  </View>
                );
              case 'tp':
                return (
                  <View>
                    <TP
                      handleFinished={handleFinished}
                      key={index}
                      quiz={quiz}
                      checkAnswer={checkAnswer}
                      handleQuizSubmit={handleQuizSubmit}
                    />
                    {showTimer && (
                      <View
                        style={[
                          t.justifyCenter,
                          t.mY4,
                          t.itemsCenter,
                          t.flexCol,
                        ]}>
                        <Timer
                          disableHoursLimit
                          duration={40}
                          handleFinished={handleFinished}
                        />
                      </View>
                    )}
                  </View>
                );
              case 'tt':
                return (
                  <View>
                    <TT
                      handleFinished={handleFinished}
                      key={index}
                      quiz={quiz}
                      checkAnswer={checkAnswer}
                      handleQuizSubmit={handleQuizSubmit}
                    />
                    {showTimer && (
                      <View
                        style={[
                          t.justifyCenter,
                          t.mY4,
                          t.itemsCenter,
                          t.flexCol,
                        ]}>
                        <Timer
                          disableHoursLimit
                          duration={40}
                          handleFinished={handleFinished}
                        />
                      </View>
                    )}
                  </View>
                );
              case 'pt':
                return (
                  <View>
                    <PT
                      handleFinished={handleFinished}
                      key={index}
                      quiz={quiz}
                      checkAnswer={checkAnswer}
                      handleQuizSubmit={handleQuizSubmit}
                    />
                    {showTimer && (
                      <View
                        style={[
                          t.justifyCenter,
                          t.mY4,
                          t.itemsCenter,
                          t.flexCol,
                        ]}>
                        <Timer
                          disableHoursLimit
                          duration={40}
                          handleFinished={handleFinished}
                        />
                      </View>
                    )}
                  </View>
                );
              default:
                return (
                  <View>
                    <Text>No format found</Text>
                  </View>
                );
            }
          })}
        </View>
      </View>
    </ScrollView>
  );
};

export default Contests;
