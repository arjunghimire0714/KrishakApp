import Button from '@components/shared/Button';
import Text from '@components/shared/Text';
import {CorrectIcon, TwentyCoinIcon} from '@images';
import React from 'react';
import {ScrollView, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {CoinUpSvg} from '@images';

const Correct = ({route, navigation: {navigate}}) => {
  const {quizId} = route.params;

  return (
    <ScrollView contentContainerStyle={[t.flexGrow]} style={[t.bgColor1, t.p4]}>
      <View
        style={[
          t.bgColor1,
          t.flex1,
          t.itemsCenter,
          t.justifyBetween,
          t.flexWrap,
        ]}>
        <View style={[t.bgColor1, t.flex, t.itemsCenter, t.justifyBetween]}>
          <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
            बधाई छ !
          </Text>
          <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
            तपाईको जवाफ सहि थियो
          </Text>
        </View>
        <View style={[t.wFull]}>
          <View
            style={[
              t.bgColor5,
              t.wFull,
              t.itemsCenter,
              t.justifyCenter,
              t.flexRow,
              t.roundedLg,
              t.p6,
              t.mB6,
            ]}>
            <View style={[t.mL5]}>
              <CoinUpSvg width={160} height={160} />
            </View>
          </View>
          <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
            तपाईले १ वटा कोइन पाउनु भएको छ
          </Text>
        </View>
        <View style={[t.flexCol, t.wFull]}>
          <Button
            btnStyle={[t.bgWhite, t.mY3, t.wFull]}
            textStyle={[t.textColor1]}
            onPress={() =>
              navigate('Winner', {
                quizId,
              })
            }
            title="आफ्नो कोइन सङ्कलन गर्नुहोस्"
          />
          <Button
            btnStyle={[t.bgWhite, t.wFull]}
            textStyle={[t.textColor1]}
            onPress={() => navigate('Main')}
            title="फिर्ता जानुहोस्"
          />
        </View>
      </View>
    </ScrollView>
  );
};

export default Correct;
