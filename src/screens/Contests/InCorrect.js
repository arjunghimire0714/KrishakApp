import Button from '@components/shared/Button';
import Text from '@components/shared/Text';
import {InCorrectIcon} from '@images';
import React from 'react';
import {ScrollView, View} from 'react-native';
import {t} from 'react-native-tailwindcss';

const InCorrect = ({navigation: {navigate}}) => {
  return (
    <ScrollView contentContainerStyle={[t.flexGrow]} style={[t.bgColor1, t.p4]}>
      <View
        style={[
          t.bgColor1,
          t.flex1,
          t.itemsCenter,
          t.justifyBetween,
          t.flexWrap,
        ]}>
        <View style={[t.bgColor1, t.flex, t.itemsCenter, t.justifyBetween]}>
          <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
            माफ गर्नुहोस् !
          </Text>
          <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
            तपाईको जवाफ गलत थियो
          </Text>
        </View>
        <View style={[t.bgColor5, t.wFull, t.itemsCenter, t.roundedLg, t.p6]}>
          <InCorrectIcon />
        </View>
        <Button
          btnStyle={[t.bgWhite, t.wFull]}
          textStyle={[t.textColor1]}
          onPress={() => navigate('Main')}
          title="फिर्ता जानुहोस्"
        />
      </View>
    </ScrollView>
  );
};

export default InCorrect;
