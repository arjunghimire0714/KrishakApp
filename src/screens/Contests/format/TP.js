import Text from '@components/shared/Text';
import MasonryList from '@react-native-seoul/masonry-list';
import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {englishToNepaliNumber} from 'nepali-number';

const TP = ({quiz, handleQuizSubmit, checkAnswer}) => {
  return (
    <View style={[]}>
      <View style={[t.pX10, t.pY3, t.bgColor1, t.flex1, t.h56, t.mB4]}>
        <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
          {quiz?.text_question}
        </Text>
      </View>
      <MasonryList
        style={t.selfStretch}
        contentContainerStyle={[t.pX5, t.selfStretch]}
        numColumns={2}
        data={quiz?.quiz_answers}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, i}) => (
          <TouchableOpacity
            disabled={checkAnswer.selected}
            onPress={() =>
              handleQuizSubmit(item.image_answer, item.is_correct, i, item.id)
            }
            key={item.id}>
            <ImageBackground
              source={{
                uri: item?.image_answer_url || '',
              }}
              imageStyle={[t.roundedLg]}
              style={[t.w40, t.h32, t.mY2, t.relative]}>
              <View
                style={[
                  t.absolute,
                  t.border,
                  styles.card,
                  t.w16,
                  t.bgWhite,
                  t.h16,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.border4,
                  t.borderWhite,
                  t.roundedFull,
                ]}>
                <View
                  style={[
                    t.border,
                    t.w12,
                    t.bgWhite,
                    t.h12,
                    t.justifyCenter,
                    t.itemsCenter,
                    t.border2,
                    t.borderBorder,
                    t.roundedFull,
                  ]}>
                  <Text style={[t.text2xl, t.fontB, t.textColor2]}>
                    {englishToNepaliNumber(i + 1)}
                  </Text>
                </View>
                {checkAnswer.selected && item.is_correct && (
                  <View
                    style={[
                      t.absolute,
                      t.border,
                      t.w12,
                      t.bgBgColor1,
                      t.h12,
                      t.justifyCenter,
                      t.itemsCenter,
                      t.border2,
                      t.roundedFull,
                      t.borderColor1,
                    ]}>
                    <Text style={[t.text2xl, t.fontB, t.textColor1]}>
                      {englishToNepaliNumber(i + 1)}
                    </Text>
                  </View>
                )}
                {checkAnswer.selected &&
                  !item.is_correct &&
                  checkAnswer.index === i && (
                    <View
                      style={[
                        t.absolute,
                        t.border,
                        t.w12,
                        t.bgBgColor2,
                        t.h12,
                        t.justifyCenter,
                        t.itemsCenter,
                        t.border2,
                        t.borderColor6,
                        t.roundedFull,
                      ]}>
                      <Text style={[t.text2xl, t.fontB, t.textColor6]}>
                        {englishToNepaliNumber(i + 1)}
                      </Text>
                    </View>
                  )}
              </View>
            </ImageBackground>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    top: -15,
    left: -15,
  },
});
export default TP;
