import Text from '@components/shared/Text';
import MasonryList from '@react-native-seoul/masonry-list';
import React from 'react';
import {
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {englishToNepaliNumber} from 'nepali-number';

const PT = ({quiz, handleQuizSubmit, checkAnswer}) => {
  return (
    <View style={[]}>
      <View style={[t.pX10, t.pY3, t.bgColor1, t.flex, t.h48, t.mB4]}>
        <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
          {quiz?.text_question}
        </Text>
      </View>
      <View style={[t.flex, t.justifyCenter, t.itemsCenter]}>
        <ImageBackground
          source={{
            uri: quiz?.image_question_url,
          }}
          imageStyle={[t.roundedLg]}
          style={[
            t.w48,
            t.h32,
            t.mY2,
            t.border4,
            t.roundedLg,
            t.bgWhite,
            t.borderWhite,
            t._mT20,
            t.mY3,
          ]}
        />
      </View>
      <MasonryList
        style={t.selfStretch}
        contentContainerStyle={[t.pX5, t.selfStretch]}
        numColumns={1}
        data={quiz?.quiz_answers}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, i}) => {
          return (
            <TouchableOpacity
              disabled={checkAnswer.selected}
              onPress={() =>
                handleQuizSubmit(item.text_answer, item.is_correct, i, item.id)
              }
              key={item.id}>
              <View
                style={[
                  t.border2,
                  t.p4,
                  t.roundedFull,
                  t.borderBorder,
                  t.mY2,
                  t.relative,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.bgColor3,
                ]}>
                <Text style={[t.textColor4, t.textBase, t.fontSb]}>
                  {item?.text_answer}
                </Text>
                <View
                  style={[
                    t.absolute,
                    t.border,
                    styles.card,
                    t.w12,
                    t.bgWhite,
                    t.h12,
                    t.justifyCenter,
                    t.itemsCenter,
                    t.border2,
                    t.roundedFull,
                    t.borderBorder,
                  ]}>
                  <Text style={[t.text2xl, t.fontB, t.textColor2]}>
                    {englishToNepaliNumber(i + 1)}
                  </Text>
                </View>
                {checkAnswer.selected && item.is_correct && (
                  <View
                    style={[
                      t.absolute,
                      t.border,
                      styles.card,
                      t.w12,
                      t.bgBgColor1,
                      t.h12,
                      t.justifyCenter,
                      t.itemsCenter,
                      t.border2,
                      t.roundedFull,
                      t.borderColor1,
                    ]}>
                    <Text style={[t.text2xl, t.fontB, t.textColor1]}>
                      {englishToNepaliNumber(i + 1)}
                    </Text>
                  </View>
                )}
                {checkAnswer.selected &&
                  !item.is_correct &&
                  checkAnswer.index === i && (
                    <View
                      style={[
                        t.absolute,
                        t.border,
                        styles.card,
                        t.w12,
                        t.bgBgColor2,
                        t.h12,
                        t.justifyCenter,
                        t.itemsCenter,
                        t.border2,
                        t.borderColor6,
                        t.roundedFull,
                      ]}>
                      <Text style={[t.text2xl, t.fontB, t.textColor6]}>
                        {englishToNepaliNumber(i + 1)}
                      </Text>
                    </View>
                  )}
              </View>
            </TouchableOpacity>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    top: 5,
    left: 5,
  },
});
export default PT;
