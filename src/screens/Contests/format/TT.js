import Text from '@components/shared/Text';
import MasonryList from '@react-native-seoul/masonry-list';
import React from 'react';
import {StyleSheet, TouchableOpacity, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {englishToNepaliNumber} from 'nepali-number';

const TT = ({quiz, handleQuizSubmit, checkAnswer}) => {
  return (
    <View style={[]}>
      <View style={[t.pX10, t.pY3, t.bgColor1, t.flex1, t.h56, t.mB4]}>
        <Text style={[t.text2xl, t.fontB, t.textCenter, t.textWhite]}>
          {quiz?.text_question}
        </Text>
      </View>
      <MasonryList
        style={t.selfStretch}
        contentContainerStyle={[t.pX5, t.selfStretch]}
        numColumns={1}
        data={quiz?.quiz_answers}
        keyExtractor={(_, index) => index.toString()}
        renderItem={({item, i}) => (
          <TouchableOpacity
            disabled={checkAnswer.selected}
            onPress={() =>
              handleQuizSubmit(item.text_answer, item.is_correct, i, item.id)
            }
            key={item.id}>
            <View
              style={[
                t.border2,
                t.p4,
                t.roundedFull,
                t.borderBorder,
                t.mY2,
                t.relative,
                t.justifyCenter,
                t.itemsCenter,
                t.bgColor3,
              ]}>
              <Text style={[t.textColor4, t.textBase, t.fontSb]}>
                {item.text_answer}
              </Text>
              <View
                style={[
                  t.absolute,
                  t.border,
                  styles.card,
                  t.w12,
                  t.bgWhite,
                  t.h12,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.border2,
                  t.roundedFull,
                  t.borderBorder,
                ]}>
                <Text style={[t.text2xl, t.fontB, t.textColor2]}>
                  {englishToNepaliNumber(i + 1)}
                </Text>
              </View>
              {checkAnswer.selected && item.is_correct && (
                <View
                  style={[
                    t.absolute,
                    t.border,
                    styles.card,
                    t.w12,
                    t.bgBgColor1,
                    t.h12,
                    t.justifyCenter,
                    t.itemsCenter,
                    t.border2,
                    t.roundedFull,
                    t.borderColor1,
                  ]}>
                  <Text style={[t.text2xl, t.fontB, t.textColor1]}>
                    {englishToNepaliNumber(i + 1)}
                  </Text>
                </View>
              )}
              {checkAnswer.selected &&
                !item.is_correct &&
                checkAnswer.index === i && (
                  <View
                    style={[
                      t.absolute,
                      t.border,
                      styles.card,
                      t.w12,
                      t.bgBgColor2,
                      t.h12,
                      t.justifyCenter,
                      t.itemsCenter,
                      t.border2,
                      t.borderColor6,
                      t.roundedFull,
                    ]}>
                    <Text style={[t.text2xl, t.fontB, t.textColor6]}>
                      {englishToNepaliNumber(i + 1)}
                    </Text>
                  </View>
                )}
            </View>
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    top: 5,
    left: 5,
  },
});
export default TT;
