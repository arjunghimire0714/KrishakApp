import Header from '@components/shared/Header';
import Loader from '@components/shared/Loader';
import Text from '@components/shared/Text';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import {useIsFocused} from '@react-navigation/native';
import getVideoId from 'get-video-id';
import React, {useEffect} from 'react';
import {Image, ScrollView, TouchableOpacity, View} from 'react-native';
import Swiper from 'react-native-swiper';
import {t} from 'react-native-tailwindcss';
import YoutubePlayer from 'react-native-youtube-iframe';
import CustomToast from '../../helpers/toast';

const DetailsSuggestions = ({route, navigation: {navigate}}) => {
  const isFocused = useIsFocused();

  const {id} = route.params;
  const [loading, setLoading] = React.useState(true);
  const [suggestion, setSuggestion] = React.useState({});
  const [suggestions, setSuggestions] = React.useState([]);

  const setReaction = async (reacts, suggestionId) => {
    try {
      setLoading(true);
      const responseReactions = await $axios.post(
        URLS.SET_ARTICLES_REACTIONS_URL(suggestionId),
        {
          reacts,
        },
      );
      if (responseReactions.data?.message) {
        CustomToast(
          'प्रतिक्रिया दिनुभएकोमा धन्यबाद। हजुरको प्रतिक्रिया रेकड भएको छ। ',
        );
      } else {
        CustomToast('तपाईंले पहिले नै प्रतिक्रिया दिनुभएको छ');
      }
      setLoading(false);
    } catch (error) {
      setLoading(false);
      CustomToast('केहि गलत भयो');
    }
  };

  const getSuggestions = async () => {
    try {
      const responseTrainings = await $axios.get(URLS.GET_ARTICLES_URL());
      setSuggestions(responseTrainings.data.results);
    } catch (error) {}
  };

  const getSuggestion = async () => {
    try {
      setLoading(true);
      const responseSuggestion = await $axios.get(URLS.GET_ARTICLE_URL(id));
      setLoading(false);
      setSuggestion(responseSuggestion.data);
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getSuggestion();
    getSuggestions();
  }, [isFocused]);

  const videoId =
    suggestion?.video_content !== ''
      ? getVideoId(suggestion?.video_content || 'SyjAhqU0Wp8')?.id
      : 'SyjAhqU0Wp8';
  return (
    <View style={[t.flex1, t.bgWhite]}>
      <View style={[t.pX4, t.pY2]}>
        <Header title="सुझाव" />
      </View>
      <Loader loading={loading} />
      <ScrollView style={[t.bgWhite]}>
        <View style={[t.p4]}>
          <View style={[t.mY2, t.pB3, t.borderBorder]}>
            {suggestion?.image_files?.length > 0 && (
              <Swiper
                style={[t.h56]}
                buttonWrapperStyle={[t.textWhite]}
                showsButtons
                showsPagination={false}
                loop={false}>
                {suggestion?.image_files?.map(item => (
                  <View key={item.id}>
                    <Image
                      style={[t.wFull, t.h56, t.roundedLg, t.mB2]}
                      source={{
                        uri: item.picture,
                      }}
                    />
                  </View>
                ))}
              </Swiper>
            )}
            <Text style={[t.text2xl, t.fontSb, t.textMediumBlack]}>
              {suggestion?.title}
            </Text>
            {suggestion?.video_content !== '' && (
              <YoutubePlayer
                webViewStyle={{opacity: 0.99}}
                height={224}
                play={false}
                videoId={videoId}
              />
            )}
            <Text style={[t.textLg, t.fontR, t.textText]}>
              {suggestion?.content}
            </Text>
          </View>
          <View style={[t.flex, t.flexRow, t.mB4]}>
            {suggestion?.tags?.map(item => (
              <View
                key={item.id}
                style={[
                  t.border2,
                  t.borderColor1,
                  t.bgBgColor1,
                  t.pX4,
                  t.pY1,
                  t.roundedFull,
                  t.mR3,
                ]}>
                <Text style={[t.textColor1, t.fontSb]}>{item}</Text>
              </View>
            ))}
          </View>
          <Text style={[t.textBase, t.fontSb]}>
            तपाईलाई यो सुझाव कस्तो लाग्यो ?
          </Text>
          <View style={[t.flex, t.flexRow, t.mY4]}>
            {[
              {id: 1, label: 'बेक्कारको', color: '#FF7272'},
              {id: 2, label: 'ठिकै', color: '#E9D529'},
              {
                id: 3,
                label: 'ज्ञान मूलक',
                color: '#469FD8',
              },
            ].map(item => (
              <TouchableOpacity
                onPress={() => setReaction(item.id, suggestion?.id)}
                key={item.id}
                style={[
                  t.bgBgColor1,
                  t.pX6,
                  t.pY3,
                  t.roundedFull,
                  t.mR3,
                  {backgroundColor: item.color},
                ]}>
                <Text style={[t.textWhite, t.fontSb]}>{item.label}</Text>
              </TouchableOpacity>
            ))}
          </View>
        </View>
        <View style={[t.borderB2, t.mY3, t.borderBorder]} />
        <View style={[t.p4]}>
          <Text style={[t.textBase, t.fontSb]}>यस्तै खाले अरु सुझावहरु</Text>
          {suggestion &&
            suggestions
              ?.filter(s => s?.id !== suggestion?.id)
              .map(item => (
                <View
                  key={item.id}
                  style={[t.mY2, t.borderB, t.pB3, t.borderBorder]}>
                  {item?.image_files?.[0] && (
                    <Image
                      style={[t.wFull, t.roundedLg, t.mB2]}
                      source={{
                        uri: item?.image_files?.[0]?.picture,
                      }}
                    />
                  )}
                  <Text style={[t.text2xl, t.fontSb, t.textMediumBlack]}>
                    {item.title}
                  </Text>
                  <Text
                    numberOfLines={2}
                    ellipsizeMode="tail"
                    style={[t.textLg, t.fontR, t.textText]}>
                    {item.content}
                  </Text>
                  <TouchableOpacity
                    onPress={() => setSuggestion(item)}
                    style={[t.p0, t.m0]}>
                    <Text style={[t.textColor1, t.fontR, t.textBase]}>
                      बृस्तित रुपमा पढ्नुहोस
                    </Text>
                  </TouchableOpacity>
                </View>
              ))}
        </View>
      </ScrollView>
    </View>
  );
};

export default DetailsSuggestions;
