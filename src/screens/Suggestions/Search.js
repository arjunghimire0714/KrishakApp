/* eslint-disable react-hooks/exhaustive-deps */
import Header from '@components/shared/Header';
import Text from '@components/shared/Text';
import TextInput from '@components/shared/TextInput';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import {SearchIcon} from '@images';
import {debounce} from 'lodash';
import React, {useCallback, useEffect} from 'react';
import {Image, ScrollView, TouchableOpacity, View} from 'react-native';
import {t} from 'react-native-tailwindcss';

const SearchSuggestions = ({navigation: {navigate}}) => {
  const [suggestions, setSuggestions] = React.useState([]);

  const getSuggestions = async searchQuery => {
    try {
      const responseTrainings = await $axios.get(
        URLS.GET_ARTICLES_URL(searchQuery),
      );
      setSuggestions(responseTrainings.data.results);
    } catch (error) {}
  };

  const handleTextChange = useCallback(
    debounce(async val => {
      await getSuggestions(`?search=${val}`);
    }, 1500),
    [],
  );

  useEffect(() => {
    getSuggestions();
  }, []);

  return (
    <View style={[t.flex1, t.bgWhite]}>
      <View style={[t.pX4, t.pY2]}>
        <Header title="सबै सुझावहरु" />
      </View>
      <View style={[t.pX4]}>
        <TextInput
          autoFocus
          icon={<SearchIcon />}
          onChangeText={handleTextChange}
          placeholder="सुझावहरुको खोज गर्नुहोस्"
          style={[
            t.roundedLg,
            t.borderB,
            t.border2,
            t.textColor2,
            t.textBase,
            t.fontSb,
          ]}
        />
      </View>
      <ScrollView style={[t.bgWhite]}>
        <View style={[t.p4]}>
          {suggestions.map(item => (
            <View
              key={item.id}
              style={[t.mY2, t.borderB, t.pB3, t.borderBorder]}>
              {item?.image_files?.[0] && (
                <Image
                  style={[t.wFull, t.h48, t.roundedLg, t.mB2]}
                  source={{
                    uri: item?.image_files[0]?.picture,
                  }}
                />
              )}
              <Text style={[t.text2xl, t.fontSb, t.textMediumBlack]}>
                {item.title}
              </Text>
              <Text
                numberOfLines={2}
                ellipsizeMode="tail"
                style={[t.textLg, t.fontR, t.textText]}>
                {item.content}
              </Text>
              <TouchableOpacity
                onPress={() =>
                  navigate('DetailsSuggestions', {
                    id: item.id,
                  })
                }
                style={[t.p0, t.m0]}>
                <Text style={[t.textColor1, t.fontR, t.textBase]}>
                  बृस्तित रुपमा पढ्नुहोस
                </Text>
              </TouchableOpacity>
            </View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
};
export default SearchSuggestions;
