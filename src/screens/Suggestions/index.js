import Header from '@components/shared/Header';
import Text from '@components/shared/Text';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import {SearchIcon} from '@images';
import React, {useEffect} from 'react';
import {Image, ScrollView, TouchableOpacity, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import Button from '@components/shared/Button';

const Suggestions = ({navigation: {navigate}}) => {
  const [suggestions, setSuggestions] = React.useState([]);
  const [next, setNext] = React.useState(false);
  const [page, setPage] = React.useState(1);
  const [tags, setTags] = React.useState([]);
  const [isTag, setIsTag] = React.useState(false);

  const getSuggestions = async (searchQuery, nextStep = false) => {
    try {
      setIsTag(false);
      const responseTrainings = await $axios.get(
        URLS.GET_ARTICLES_URL(searchQuery),
      );
      if (nextStep) {
        setPage(page + 1);
        setNext(responseTrainings.data.next ? true : false);
        setSuggestions([...suggestions, ...responseTrainings.data.results]);
      } else {
        setNext(responseTrainings.data.next ? true : false);
        setSuggestions(responseTrainings.data.results);
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  const getTags = async () => {
    try {
      const responseTags = await $axios.get(URLS.GET_ARTICLE_TAGS_URL);
      setTags(responseTags.data);
    } catch (error) {}
  };

  const getSuggestionsByTags = async (searchQuery, nextStep = false) => {
    try {
      setIsTag(true);
      const responseTrainings = await $axios.get(
        URLS.GET_ARTICLE_TAGS_BY_SLUG_URL(searchQuery),
      );
      if (nextStep) {
        setPage(page + 1);
        setNext(responseTrainings.data.next ? true : false);
        setSuggestions([...suggestions, ...responseTrainings.data.results]);
      } else {
        setNext(responseTrainings.data.next ? true : false);
        setSuggestions(responseTrainings.data.results);
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  useEffect(() => {
    getSuggestions();
    getTags();
  }, []);

  return (
    <ScrollView style={[t.bgWhite]}>
      <View style={[t.flex1, t.bgWhite]}>
        <View style={[t.pX4, t.pY2]}>
          <Header title="सबै सुझावहरु" />
        </View>
        <View style={[t.pX4]}>
          <TouchableOpacity onPress={() => navigate('SearchSuggestions')}>
            <View
              style={[
                t.fontR,
                t.bgBg,
                t.p3,
                t.border2,
                t.borderBorder,
                t.textBase,
                t.roundedLg,
                t.justifyBetween,
                t.flexRow,
              ]}>
              <Text style={[t.fontSb, t.textColor2, t.textLg]}>
                सुझावहरुको खोज गर्नुहोस्
              </Text>
              <SearchIcon />
            </View>
          </TouchableOpacity>
        </View>
        <View style={[t.p4]}>
          <Text style={[t.textLg, t.fontSb, t.mY2, t.textColor2]}>
            हाम्रा प्रसिद्ध विषयहरु
          </Text>
          <View style={[t.flex, t.flexRow, t.mB4]}>
            {tags.map(item => (
              <TouchableOpacity
                key={item.id}
                onPress={() => getSuggestionsByTags(`${item.name}`, false)}
                style={[
                  t.border2,
                  t.borderColor1,
                  t.bgBgColor1,
                  t.pX4,
                  t.pY1,
                  t.roundedFull,
                  t.mR3,
                ]}>
                <Text style={[t.textColor1, t.fontSb]}>{item.name}</Text>
              </TouchableOpacity>
            ))}
          </View>
          {suggestions.map(item => {
            return (
              <View
                key={item.id}
                style={[t.mY2, t.borderB, t.pB3, t.borderBorder]}>
                {item?.image_files?.[0] && (
                  <Image
                    style={[t.wFull, t.h48, t.roundedLg, t.mB2]}
                    source={{
                      uri: item?.image_files?.[0]?.picture,
                    }}
                  />
                )}
                <Text style={[t.text2xl, t.fontSb, t.textMediumBlack]}>
                  {item.title}
                </Text>
                <Text
                  numberOfLines={2}
                  ellipsizeMode="tail"
                  style={[t.textLg, t.fontR, t.textText]}>
                  {item.content}
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    navigate('DetailsSuggestions', {
                      id: item.id,
                    })
                  }
                  style={[t.p0, t.m0]}>
                  <Text style={[t.textColor1, t.fontR, t.textBase]}>
                    बृस्तित रुपमा पढ्नुहोस
                  </Text>
                </TouchableOpacity>
              </View>
            );
          })}
          {next && (
            <Button
              onPress={() =>
                isTag
                  ? getSuggestionsByTags(`?page=${page + 1}`, next)
                  : getSuggestions(`?page=${page + 1}`, next)
              }
              title="सबै सुझावहरु पढ्नुहोस्"
            />
          )}
        </View>
      </View>
    </ScrollView>
  );
};
export default Suggestions;
