import Header from '@components/shared/Header';
import Text from '@components/shared/Text';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import moment from 'moment';
import React, {useEffect} from 'react';
import {ScrollView, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {SelectableText} from '@astrocoders/react-native-selectable-text';
import Loader from '@components/shared/Loader';
import Clipboard from '@react-native-community/clipboard';
import Button from '@components/shared/Button';

const Notifications = ({navigation: {navigate}}) => {
  const [notifications, setNotifications] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const [next, setNext] = React.useState(false);
  const [page, setPage] = React.useState(1);

  const getNotifications = async (searchQuery, nextStep = false) => {
    try {
      setLoading(true);
      const responseNotifications = await $axios.get(
        URLS.GET_NOTIFICATIONS_URL(searchQuery),
      );
      setLoading(false);
      if (nextStep) {
        setPage(page + 1);
        setNext(responseNotifications.data.next ? true : false);
        setNotifications([
          ...notifications,
          ...responseNotifications.data.results,
        ]);
      } else {
        setNext(responseNotifications.data.next ? true : false);
        setNotifications(responseNotifications.data.results);
      }
    } catch (error) {
      setLoading(false);
    }
  };

  useEffect(() => {
    getNotifications();
  }, []);
  return (
    <View style={[t.flex1, t.bgWhite]}>
      <View style={[t.pX4, t.pY2]}>
        <Header title="तपाईको सूचना" />
      </View>
      <Loader loading={loading} />
      <ScrollView style={[t.bgWhite]}>
        <View style={[t.p4]}>
          {notifications.map((n, index) => {
            return (
              <View key={index} style={[t.pY2]}>
                <View style={[t.flexRow, t.justifyBetween, t.flexWrap]}>
                  <SelectableText
                    style={[t.textLg, t.textBlack, t.fontSb, t.flexWrap]}
                    menuItems={['Copy']}
                    onSelection={({
                      eventType,
                      content,
                      selectionStart,
                      selectionEnd,
                    }) => {
                      console.log(eventType);
                      if (eventType === 'Copy') {
                        Clipboard.setString(content);
                      }
                    }}
                    value={n.notification_text}
                  />
                  <Text style={[t.textBase, t.textText, t.fontR, t.flexWrap]}>
                    {moment(
                      moment(n.push_datetime, 'MMMM Do, YYYY, h:mm:ss'),
                    ).fromNow()}
                  </Text>
                </View>
                <View style={[t.borderB, t.borderBorder]} />
              </View>
            );
          })}
          {next && (
            <Button
              onPress={() => getNotifications(`?page=${page + 1}`, next)}
              title="सबै सूचनाहरू पढ्नुहोस्"
            />
          )}
        </View>
      </ScrollView>
    </View>
  );
};
export default Notifications;
