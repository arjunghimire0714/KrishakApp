import Button from '@components/shared/Button';
import Text from '@components/shared/Text';
import Timer from '@components/shared/Timer';
import Zoom from '@components/shared/Zoom';
import {moment, times} from '@helpers';
import {$axios} from '@helpers/axios';
import {URLS} from '@helpers/urls';
import {ContestsIcon, SuggestionIcon, TrainingIcon} from '@images';
import messaging from '@react-native-firebase/messaging';
import {useIsFocused} from '@react-navigation/native';
import {authActions} from '@redux/actions';
import {englishToNepaliNumber} from 'nepali-number';
import React, {useEffect} from 'react';
import {ActivityIndicator, Image, TouchableOpacity, View} from 'react-native';
import PTRView from 'react-native-pull-to-refresh';
import {t} from 'react-native-tailwindcss';
import {useDispatch, useSelector} from 'react-redux';

const adbs = require('ad-bs-converter');

const Main = ({navigation: {navigate}}) => {
  const dispatch = useDispatch();
  const isFocused = useIsFocused();
  const [upcomingTrainings, setUpcomingTrainings] = React.useState([]);
  const [suggestions, setSuggestions] = React.useState([]);
  const [loading, setLoading] = React.useState(false);
  const {userDetails} = useSelector(state => state.auth);
  const [quizzes, setQuizzes] = React.useState([]);

  const getQuizzes = async () => {
    try {
      setLoading(true);
      const responseSuggestions = await $axios.get(URLS.GET_QUIZZES);
      setQuizzes(responseSuggestions.data.results);
      setLoading(false);
    } catch (error) {
      setLoading(false);
    }
  };

  const getSuggestions = async () => {
    try {
      setLoading(true);
      const responseSuggestions = await $axios.get(URLS.GET_ARTICLES_URL());
      setLoading(false);
      setSuggestions(responseSuggestions.data.results.slice(0, 3));
    } catch (error) {
      setLoading(false);
    }
  };

  const getUpcomingTrainings = async () => {
    try {
      const responseTrainings = await $axios.get(
        URLS.GET_UPCOMING_TRAININGS_URL(),
      );
      setUpcomingTrainings(responseTrainings.data.results?.slice(0, 3));
    } catch (error) {}
  };

  const handleRefresh = () => {
    getUpcomingTrainings();
    getSuggestions();
  };

  React.useEffect(() => {
    setLoading(false);
    messaging().onNotificationOpenedApp(remoteMessage => {
      if (remoteMessage?.data?.type === 'article') {
        navigate('DetailsSuggestions', {
          id: remoteMessage?.data?.article_id,
        });
      } else if (remoteMessage?.data?.type === 'training') {
        navigate('Training');
      } else if (remoteMessage?.data?.type === 'quiz') {
        navigate('Contests');
      } else {
        navigate('Notifications');
      }
    });

    // Check whether an initial notification is available
    messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        if (remoteMessage) {
          if (remoteMessage?.data?.type === 'article') {
            navigate('DetailsSuggestions', {
              id: remoteMessage?.data?.article_id,
            });
          } else if (remoteMessage?.data?.type === 'training') {
            navigate('Training');
          } else if (remoteMessage?.data?.type === 'quiz') {
            navigate('Contests');
          } else {
            navigate('Notifications');
          }
        }
      });
    setLoading(false);
  }, []);

  useEffect(() => {
    getQuizzes();
  }, [isFocused]);

  useEffect(() => {
    getUpcomingTrainings();
    getSuggestions();
    dispatch({
      type: authActions.USER_INFO_REQUEST,
    });
  }, [isFocused]);

  const categories = [
    {
      id: 1,
      name: 'तालिमहरु',
      route: 'Training',
      image: <TrainingIcon />,
    },
    {
      id: 2,
      name: 'सुझावहरु',
      route: 'Suggestions',
      image: <SuggestionIcon />,
    },
    {
      id: 3,
      name: 'प्रतियोगिताहरु',
      route: quizzes?.length === 0 ? 'Weekly' : 'Contests',
      image: <ContestsIcon />,
    },
  ];

  return (
    <View style={[t.flex1, t.bgWhite]}>
      <View style={[t.pX4]}>
        <Text style={[t.textBase, t.mT4, t.fontSb]}>हाम्रा विशेषताहरु</Text>
        <View
          style={[t.flex, t.flexRow, t.justifyBetween, t.mY3, t.itemsCenter]}>
          {categories.map(category => (
            <TouchableOpacity
              onPress={() => navigate(category.route)}
              key={category.id}
              style={[t.rounded]}>
              <View
                style={[
                  t.h20,
                  t.w24,
                  t.bgBg,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.roundedLg,
                ]}>
                {category.image}
              </View>
              <Text style={[t.mY3, t.fontSb, t.textCenter, t.textColor2]}>
                {category.name}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
      {loading && (
        <View style={[t.flex1, t.justifyCenter, t.itemsCenter]}>
          <ActivityIndicator size="large" color="#0000ff" />
        </View>
      )}

      {!loading && (
        <PTRView onRefresh={handleRefresh}>
          <View style={[]}>
            <Text style={[t.textLg, t.pX4, t.mB3, t.textText, t.fontSb]}>
              हाम्रो आगामि कृषि तालिम
            </Text>
            {upcomingTrainings.map(item => {
              const momentObj = moment(
                item.training_date + ' ' + item.training_time,
                'YYYY-MM-DD hh:mm:ss',
              );
              const {
                ne: {strMonth, day, strDayOfWeek},
              } = adbs.ad2bs(moment(item.training_date).format('YYYY/MM/DD'));
              const momentCurrentObj = moment().format('YYYY-MM-DD hh:mm:ss a');
              const startDate = moment(momentObj, 'YYYY-MM-DD hh:mm:ss a');
              const endDate = moment(momentCurrentObj, 'YYYY-MM-DD hh:mm:ss a');
              const diff = startDate.diff(endDate, 'seconds');
              return (
                <View key={item.id} style={[t.bgBorder, t.p4]}>
                  <Image
                    style={[t.wFull, t.h48, t.roundedLg, t.mB2]}
                    source={{
                      uri: item.thumbnail,
                    }}
                  />
                  <Text style={[t.text2xl, t.fontB, t.textBlack]}>
                    {item.title}
                  </Text>
                  <Text style={[t.textXl, t.fontSb, t.textText]}>
                    {item.description}
                  </Text>
                  <View style={[t.mY3]}>
                    <Text style={[t.textLg, t.fontR, t.textBlack]}>
                      मिति : {`${strMonth} ${day}, ${strDayOfWeek}`}
                    </Text>
                    <Text style={[t.textLg, t.fontR, t.textBlack]}>
                      समय : {times[momentObj.format('a')]}{' '}
                      {englishToNepaliNumber(momentObj.format('hh'))} :
                      {englishToNepaliNumber(momentObj.format('mm'))} बजे
                    </Text>
                  </View>
                  {item.has_registered && diff > 0 && (
                    <Button
                      disabled
                      title={
                        <Timer
                          primary
                          duration={diff}
                          handleFinished={handleRefresh}
                        />
                      }
                    />
                  )}
                  {diff < 0 && item.has_registered && (
                    <Zoom
                      id={userDetails?.id}
                      name={userDetails?.name || 'Bot'}
                      zoomLink={item?.zoom_link}
                    />
                  )}
                  {!item.has_registered && (
                    <Button
                      onPress={() =>
                        navigate('Participation', {
                          trainingId: item.id,
                        })
                      }
                      title="आफ्नो सिट बुक गर्नुहोस्"
                    />
                  )}
                </View>
              );
            })}
          </View>
          <View style={[t.pX4, t.mT3]}>
            <Text style={[t.textLg, t.fontB, t.textText]}>
              हाम्रा कृषि खुराकहरु
            </Text>
            {suggestions?.map(item => (
              <View
                key={item.id}
                style={[t.mY2, t.borderB, t.pB3, t.borderBorder]}>
                {item?.image_files?.slice(0, 1)?.map(image => (
                  <Image
                    key={image.id}
                    style={[t.wFull, t.h56, t.roundedLg, t.mB2]}
                    source={{
                      uri: image?.picture,
                    }}
                  />
                ))}
                <Text style={[t.text2xl, t.fontB, t.textBlack]}>
                  {item.title}
                </Text>
                <Text
                  numberOfLines={2}
                  ellipsizeMode="tail"
                  style={[t.textBase, t.fontR, t.textColor2]}>
                  {item.content}
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    navigate('DetailsSuggestions', {
                      id: item.id,
                    })
                  }
                  style={[t.p0, t.m0]}>
                  <Text style={[t.textColor1, t.fontR, t.textBase]}>
                    बृस्तित रुपमा पढ्नुहोस
                  </Text>
                </TouchableOpacity>
              </View>
            ))}
            {suggestions?.length > 0 && (
              <Button
                onPress={() => navigate('Suggestions')}
                title="सबै सुझावहरु पढ्नुहोस्"
              />
            )}
          </View>
          <View style={[t.mT3]}>
            <Text style={[t.textLg, t.pX4, t.textText, t.fontSb]}>
              हाम्रो सुपर कृषक प्रतियोगिता
            </Text>
            <TouchableOpacity
              onPress={() => navigate('Contests')}
              style={[t.flexRow, t.justifyCenter, t.pX4, t.itemsCenter]}>
              <Image
                style={[t.wFull, t.h48, t.roundedLg, t.mB2]}
                resizeMode="contain"
                source={require('@images/quiz-competition.png')}
              />
            </TouchableOpacity>
          </View>
        </PTRView>
      )}
    </View>
  );
};
export default Main;
