import Header from '@components/shared/Header';
import Loader from '@components/shared/Loader';
import Text from '@components/shared/Text';
import {$axios} from '@helpers/axios';
import coins from '@helpers/coin';
import {URLS} from '@helpers/urls';
import {CoinUpSvg} from '@images';
import MasonryList from '@react-native-seoul/masonry-list';
import {authActions} from '@redux/actions';
import React, {useEffect} from 'react';
import {
  ActivityIndicator,
  Image,
  ScrollView,
  TouchableOpacity,
  View,
} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {useDispatch} from 'react-redux';
import {useIsFocused} from '@react-navigation/native';
import {useNetInfo} from '@react-native-community/netinfo';
import {saveDataInStorage} from '@helpers/storage';
import {GoogleSignin} from '@react-native-community/google-signin';

const Profile = ({navigation: {navigate}}) => {
  const isFocused = useIsFocused();
  const netInfo = useNetInfo();

  const dispatch = useDispatch();
  const [user, setUser] = React.useState({});
  const [loading, setLoading] = React.useState(false);

  const getUser = async () => {
    try {
      setLoading(true);
      const resUser = await $axios.get(URLS.GET_USER_URL);
      setLoading(false);
      setUser(resUser.data);
    } catch (error) {
      setLoading(false);
    }
  };

  const handleLogout = async () => {
    try {
      if (!netInfo.isConnected) {
        await GoogleSignin.signOut();
        await saveDataInStorage('userData', {});
        dispatch({
          type: authActions.LOGOUT_SUCCESS,
          payload: {},
        });
        navigate('Login');
        return;
      }
      dispatch({
        type: authActions.LOGOUT_REQUEST,
      });
      await GoogleSignin.signOut();
    } catch (e) {
      await GoogleSignin.signOut();
    }
  };

  useEffect(() => {
    getUser();
  }, [isFocused]);

  return (
    <View style={[t.flex1, t.bgWhite]}>
      <View style={[t.pX4, t.pY2]}>
        <Header title="तपाईको खाता" />
      </View>

      <ScrollView contentContainerStyle={t.flexGrow} style={[t.bgWhite]}>
        <View style={[t.pX4, t.pY2]}>
          <View style={[t.flexRow, t.itemsCenter]}>
            <View>
              <View
                style={[
                  t.border4,
                  t.roundedFull,
                  t.justifyCenter,
                  t.itemsCenter,
                  t.flexRow,
                  t.p2,
                  t.borderColor1,
                ]}>
                <Image
                  style={[t.w24, t.h24, t.roundedFull]}
                  source={
                    user?.profile_pic || user?.profile_pic_url
                      ? {
                          uri: user?.profile_pic_url || user?.profile_pic,
                        }
                      : require('@images/logo.png')
                  }
                />
              </View>
            </View>
            <View style={[t.mL4]}>
              <Text style={[t.textXl, t.textBlack, t.fontSb]}>
                {user?.name || `SuperKrishak${user?.id || ''}`}
              </Text>
              <Text style={[t.textBase, t.textText, t.fontSb]}>
                {(user?.coin || 0) > 50 ? 'व्यावसायिक कृषक' : 'नयाँ कृषक'}
              </Text>
              <View style={[t.flexRow, t.itemsCenter]}>
                <CoinUpSvg width={30} height={30} />
                <Text style={[t.textBase, t.textText, t.fontR]}>
                  {user?.coins === 1 ||
                  user?.coins === 0 ||
                  user?.coins === null
                    ? `${user?.coins || 0} कोइन`
                    : `${user?.coins || 0} कोइनहरु`}
                </Text>
              </View>
              <TouchableOpacity onPress={handleLogout}>
                <Text style={[t.textBase, t.textRed700, t.fontSb]}>
                  लग आउट गर्नुहोस
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={[t.mY6, t.flex1]}>
            <Text style={[t.textLg, t.textText]}>तपाईको पदकहरु</Text>
            {loading && (
              <View style={[t.flex1, t.itemsCenter]}>
                <ActivityIndicator size="large" color="#0000ff" />
              </View>
            )}
            <View>
              <MasonryList
                style={t.selfStretch}
                contentContainerStyle={[t.pX0, t.selfStretch]}
                numColumns={3}
                data={coins}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, i}) => {
                  const {image1: Icon, image2: Icon2, name, score} = item;
                  return (
                    <View
                      style={[t.mX2, t.justifyCenter, t.itemsCenter, t.flexRow]}
                      key={i}>
                      <View>
                        <View
                          style={[
                            t.bgBgColor1,
                            t.mY2,
                            t.pY4,
                            t.pX6,
                            t.roundedLg,
                          ]}>
                          {user?.coins >= score ? (
                            <Icon width={60} height={60} />
                          ) : (
                            <Icon2 width={60} height={60} />
                          )}
                        </View>
                        <Text style={[t.textCenter, t.textText]}>{name}</Text>
                      </View>
                    </View>
                  );
                }}
              />
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Profile;
