import React from 'react';
import Spinner from 'react-native-loading-spinner-overlay';

const Loader = ({loading}) => <Spinner visible={loading} />;

export default Loader;
