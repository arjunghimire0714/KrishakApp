import Button from '@components/shared/Button';
import {extractDataFromJoinLink, sdkKey, sdkSecret} from '@helpers';
import React, {useEffect, useState} from 'react';
import {NativeEventEmitter, StyleSheet, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import ZoomUs, {ZoomEmitter} from 'react-native-zoom-us';
import CustomViewer from './CustomView';

const enableCustomizedMeetingUI = false;

const Zoom = ({zoomLink, name, id}) => {
  const exampleJoinMeeting = extractDataFromJoinLink(zoomLink);
  const [isInitialized, setIsInitialized] = useState(false);
  const [isMeetingOngoing, setIsMeetingOngoing] = useState(false);
  useEffect(() => {
    (async () => {
      try {
        const initializeResult = await ZoomUs.initialize(
          {clientKey: sdkKey, clientSecret: sdkSecret},
          {
            language: 'en-EN',
            enableCustomizedMeetingUI,
          },
        );

        console.log({initializeResult});

        setIsInitialized(true);
      } catch (e) {
        console.error(e);
      }
    })();
  }, []);

  useEffect(() => {
    if (!isInitialized) {
      return;
    }

    // For more see https://github.com/mieszko4/react-native-zoom-us/blob/master/docs/EVENTS.md
    const zoomEmitter = new NativeEventEmitter(ZoomEmitter);
    const eventListener = zoomEmitter.addListener(
      'MeetingEvent',
      ({event, status, ...params}) => {
        console.log({event, status, params}); //e.g.  "endedByHost" (see more: https://github.com/mieszko4/react-native-zoom-us/blob/master/docs/EVENTS.md)

        if (status === 'MEETING_STATUS_CONNECTING') {
          setIsMeetingOngoing(true);
        }

        if (status === 'MEETING_STATUS_DISCONNECTING') {
          // Once it is set it is good to render
          setIsMeetingOngoing(false);
        }
      },
    );

    return () => eventListener.remove();
  }, [isInitialized]);

  const joinMeeting = async () => {
    try {
      await ZoomUs.joinMeeting({
        autoConnectAudio: true,
        userName: `${name}_${id}`,
        meetingNumber: exampleJoinMeeting.meetingNumber || '',
        password: exampleJoinMeeting.password || '',
      });
    } catch (e) {
      console.error(e);
    }
  };

  const leaveMeeting = async () => {
    try {
      const leaveMeetingResult = await ZoomUs.leaveMeeting();

      console.log({leaveMeetingResult});
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <>
      <View style={[t.flex]}>
        <Button
          onPress={() => joinMeeting()}
          title="जोडिनुहोस"
          disabled={!isInitialized}
        />
      </View>
      {enableCustomizedMeetingUI && isMeetingOngoing && (
        <CustomViewer leaveMeeting={leaveMeeting} />
      )}
    </>
  );
};

export default Zoom;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  customViewer: {
    width: '100%',
    flex: 1,
    backgroundColor: 'red',
  },
});
