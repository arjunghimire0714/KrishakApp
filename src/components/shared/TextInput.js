import {PasswordShowEye, PasswordHideEye} from '@images';
import React from 'react';
import {
  StyleSheet,
  TextInput as RNTextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import {t} from 'react-native-tailwindcss';
import tailwindConfig from '../../../tailwind.config';

const WIDTH = 20;
const HEIGHT = 20;

const TextInput = ({
  placeholder,
  onChangeText,
  style = [],
  icon,
  showPassword = false,
  errorMessage,
  handleIconPress,
  secureTextEntry,
  ...restProps
}) => {
  return (
    <View style={t.relative}>
      <RNTextInput
        secureTextEntry={secureTextEntry}
        placeholder={placeholder}
        placeholderTextColor={
          errorMessage
            ? tailwindConfig.theme.extend.colors.color6
            : tailwindConfig.theme.extend.colors.color2
        }
        onChangeText={onChangeText}
        style={[
          t.fontR,
          t.bgBg,
          t.p3,
          t.border2,
          t.borderBorder,
          t.textBase,
          ...style,
          errorMessage
            ? [t.borderColor6, t.bgBgColor2]
            : [t.textBase, t.textText],
        ]}
        {...restProps}
      />
      {icon && (
        <TouchableOpacity
          onPress={handleIconPress}
          style={[t.absolute, styles.position, t.justifyCenter, t.itemsCenter]}>
          {icon}
        </TouchableOpacity>
      )}
      {showPassword && (
        <TouchableOpacity
          onPress={handleIconPress}
          style={[t.absolute, styles.position, t.justifyCenter, t.itemsCenter]}>
          {secureTextEntry ? (
            <PasswordHideEye height={HEIGHT} width={WIDTH} />
          ) : (
            <PasswordShowEye height={HEIGHT} width={WIDTH} />
          )}
        </TouchableOpacity>
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  position: {
    right: 20,
    bottom: 20,
  },
});

export default TextInput;
