import React from 'react';
import {View} from 'react-native';
import CountDown from 'react-native-countdown-component';
import {t} from 'react-native-tailwindcss';

const Timer = ({
  handleFinished,
  primary,
  duration = 40,
  disableHoursLimit = false,
}) => {
  return (
    <View style={[primary ? t.bgColor1 : t.bgBg, t.roundedLg]}>
      <CountDown
        until={duration}
        onFinish={handleFinished}
        size={primary ? 15 : 20}
        disableHoursLimit={disableHoursLimit}
        digitStyle={{
          backgroundColor: primary ? '#469FD8' : '#F6F6F6',
          fontFamily: 'Baloo2-Regular',
        }}
        timeToShow={disableHoursLimit ? ['S'] : ['D', 'H', 'M', 'S']}
        timeLabels={{m: null, s: null}}
        digitTxtStyle={{
          color: primary ? '#fff' : '#000',
          fontFamily: 'Baloo2-Regular',
          fontSize: 18,
        }}
        separatorStyle={primary ? t.textWhite : t.textBlack}
        showSeparator
      />
    </View>
  );
};

export default Timer;
