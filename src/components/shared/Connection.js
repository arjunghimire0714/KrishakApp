import React from 'react';
import {Text, TouchableOpacity} from 'react-native';
import DeviceSettings from 'react-native-device-settings';
import {t} from 'react-native-tailwindcss';

const Connection = () => {
  return (
    <TouchableOpacity
      onPress={() => DeviceSettings.open()}
      style={[
        t.itemsCenter,
        t.justifyCenter,
        t.flexRow,
        t.p2,
        t.wFull,
        t.bgNetwork,
      ]}>
      <Text style={[t.textWhite, t.fontR, t.textCenter, t.fontNormal]}>
        इन्टरनेट जडान छैन । कृपया आफ्नो जडान जाँच गर्नुहोस् र पुन: प्रयास
        गर्नुहोस्
      </Text>
    </TouchableOpacity>
  );
};

export default Connection;
