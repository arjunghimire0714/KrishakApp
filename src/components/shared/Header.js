import React from 'react';
import {BackArrow} from '@images';
import {Text, TouchableOpacity, View} from 'react-native';
import {t} from 'react-native-tailwindcss';
import {CommonActions, useNavigation} from '@react-navigation/native';

const Header = ({title}) => {
  const navigation = useNavigation();
  return (
    <View style={[t.flex, t.mT3, t.flexRow, t.itemsCenter]}>
      <TouchableOpacity
        onPress={() => navigation.dispatch(CommonActions.goBack())}>
        <BackArrow />
      </TouchableOpacity>
      <Text style={[t.text2xl, t.mL6, t.fontB, t.textColor2]}>{title}</Text>
    </View>
  );
};

export default Header;
