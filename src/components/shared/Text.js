import React from 'react';
import {Text as RNText} from 'react-native';
import {t} from 'react-native-tailwindcss';

const Text = ({children, style = [], ...props}) => {
  return (
    <RNText style={[t.fontR, ...style]} {...props}>
      {children}
    </RNText>
  );
};

export default Text;
