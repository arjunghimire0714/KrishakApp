import React from 'react';
import {TouchableOpacity} from 'react-native';
import Text from './Text';
import {t} from 'react-native-tailwindcss';

const Button = ({title, onPress, disabled, btnStyle = [], textStyle = []}) => {
  return (
    <TouchableOpacity
      disabled={disabled}
      style={[
        t.bgColor1,
        t.flex,
        t.flexRow,
        t.justifyCenter,
        t.itemsCenter,
        t.p2,
        t.roundedLg,
        ...btnStyle,
      ]}
      onPress={onPress}>
      <Text style={[t.text2xl, t.fontB, t.textWhite, ...textStyle]}>
        {title}
      </Text>
    </TouchableOpacity>
  );
};

export default Button;
