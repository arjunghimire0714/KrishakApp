import React from 'react';
import {Text, View} from 'react-native';
import CountDown from 'react-native-countdown-component';
import {t} from 'react-native-tailwindcss';

const Timer = ({
  handleFinished,
  primary,
  duration = 40,
  disableHoursLimit = false,
}) => {
  return (
    <View style={[t.bgBg, t.flexRow, t.itemsCenter, t.roundedLg]}>
      <Text style={[t.textBlack, t.pL2, t.fontB, {fontSize: 18}]}>समय :</Text>
      <CountDown
        until={duration}
        onFinish={handleFinished}
        size={primary ? 15 : 20}
        disableHoursLimit={disableHoursLimit}
        digitStyle={{
          backgroundColor: '#F6F6F6',
          fontFamily: 'Baloo2-Regular',
        }}
        timeToShow={disableHoursLimit ? ['S'] : ['D', 'H', 'M', 'S']}
        timeLabels={{m: null, s: null}}
        digitTxtStyle={{
          color: '#000',
          fontFamily: 'Baloo2-Regular',
          fontSize: 18,
        }}
        separatorStyle={primary ? t.textWhite : t.textBlack}
        showSeparator
      />
    </View>
  );
};

export default Timer;
