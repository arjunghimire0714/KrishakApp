import {$axios} from '@helpers/axios';
import {loadStorageData, saveDataInStorage} from '@helpers/storage';
import Toast from '@helpers/toast';
import {URLS} from '@helpers/urls';
import {all, call, fork, put, select, takeEvery} from 'redux-saga/effects';
import * as RootNavigation from '../../navigation';
import {authActions} from '../actions';

const getAuthState = state => state.auth;

export function* checkUserLogin() {
  yield takeEvery(authActions.CHECK_LOGIN_REQUEST, function* () {
    try {
      const payload = yield call(loadStorageData, 'userData');
      const sendData = {
        ...payload,
      };
      yield put({
        type: authActions.CHECK_LOGIN_SUCCESS,
        payload: sendData || {},
      });
    } catch (error) {
      yield put({
        type: authActions.CHECK_LOGIN_FAILED,
      });
    }
  });
}

export function* logIn() {
  yield takeEvery(authActions.LOGIN_REQUEST, function* ({data}) {
    try {
      let response;
      if (data.type === 'google') {
        const sendData = {
          id_token: data.tokenId,
        };
        response = yield call(
          $axios.post,
          URLS.GOOGLE_REGISTRATION_URL,
          sendData,
        );
        const userDetails = {
          ...response.data,
        };
        yield call(saveDataInStorage, 'userData', userDetails);
        yield put({
          type: authActions.LOGIN_SUCCESS,
          payload: userDetails,
        });
      } else if (data.type === 'facebook') {
        const sendData = {
          auth_token: data.auth_token,
        };
        response = yield call(
          $axios.post,
          URLS.FACEBOOK_REGISTRATION_URL,
          sendData,
        );
        const userDetails = {
          ...response.data,
        };
        yield call(saveDataInStorage, 'userData', userDetails);
        yield put({
          type: authActions.LOGIN_SUCCESS,
          payload: userDetails,
        });
      } else {
        const sendData = {
          email: data.email,
          password: data.password,
        };
        response = yield call($axios.post, URLS.LOGIN_URL, sendData);
        const userDetails = {
          ...response.data,
        };
        yield call(saveDataInStorage, 'userData', userDetails);
        yield put({
          type: authActions.LOGIN_SUCCESS,
          payload: userDetails,
        });
      }
    } catch (error) {
      console.log('ERROR', error?.response?.data);
      yield put({
        type: authActions.LOGIN_FAILED,
        payload: 'तपाईको पासवर्ड वा फोन / एमेल मिलेन',
      });
    }
  });
}

export function* signUp() {
  yield takeEvery(authActions.REGISTER_REQUEST, function* ({data}) {
    try {
      const response = yield call($axios.post, URLS.SIGNUP_URL, data);
      if (response.data?.detail) {
        Toast(response.data?.detail);
        RootNavigation.navigate('Verify');
        yield put({
          type: authActions.REGISTER_SUCCESS,
          payload: data,
        });
      }
    } catch (error) {
      console.log('ERROR', error.response, data);
    }
  });
}

export function* verifyEmail() {
  yield takeEvery(authActions.VERIFY_REQUEST, function* ({data}) {
    try {
      const response = yield call($axios.post, URLS.VERIFY_URL, data);
      if (response.data?.detail) {
        Toast(response.data?.detail);
        yield put({
          type: authActions.LOGIN_SUCCESS,
          payload: {
            ...response.data,
            isProcess: true,
          },
        });
        yield call(saveDataInStorage, 'userData', response.data);
      }
    } catch (error) {
      const message = error.response?.data?.otp_code;
      Toast(message);
      yield put({
        type: authActions.VERIFY_FAILED,
      });
    }
  });
}

export function* logOut() {
  yield takeEvery(authActions.LOGOUT_REQUEST, function* () {
    try {
      yield call(saveDataInStorage, 'userData', {});
      yield put({
        type: authActions.LOGOUT_SUCCESS,
        payload: {},
      });
      RootNavigation.navigate('Login');
    } catch (error) {
      yield call(saveDataInStorage, 'userData', {});
      yield put({
        type: authActions.LOGOUT_SUCCESS,
        payload: {},
      });
      RootNavigation.navigate('Login');
      yield put({
        type: authActions.LOGOUT_FAILED,
      });
    }
  });
}

export function* userInfo() {
  yield takeEvery(authActions.USER_INFO_REQUEST, function* () {
    try {
      const payload = yield call(loadStorageData, 'userData');
      const response = yield call($axios.get, URLS.GET_USER_URL);
      const sendData = {
        ...payload,
        ...response.data,
      };
      yield put({
        type: authActions.USER_INFO_SUCCESS,
        payload: sendData,
      });
    } catch (error) {
      yield put({
        type: authActions.USER_INFO_FAILED,
      });
    }
  });
}

export function* forgotPassword() {
  yield takeEvery(authActions.FORGOT_PASSWORD_REQUEST, function* ({data}) {
    try {
      yield call($axios.post, URLS.FORGOT_PASSWORD_URL, data);
      yield put({
        type: authActions.FORGOT_PASSWORD_SUCCESS,
        payload: 'पासवर्ड रिसेट लिङ्क तपाईंको इमेलमा पठाइएको छ ',
      });
    } catch (error) {
      yield put({
        type: authActions.FORGOT_PASSWORD_FAILED,
        payload: 'त्यस विवरण खाता सिर्जना भएको छैन',
      });
    }
  });
}

export function* registerTokenFCM() {
  yield takeEvery(
    authActions.REGISTER_NOTIFICATION_TOKEN_REQUEST,
    function* ({data}) {
      try {
        const response = yield call(loadStorageData, 'userData');
        const sendData = {
          ...response,
          registerToken: data,
        };
        yield call(saveDataInStorage, 'userData', sendData);
        yield put({
          type: authActions.SET_NOTIFICATION_TOKEN_SUCCESS,
          payload: sendData,
        });
        const {registerToken, isFirebaseTokenRegistered} = yield call(
          loadStorageData,
          'userData',
        );
        if (registerToken && !isFirebaseTokenRegistered) {
          const sendRequest = {
            registration_id: data,
            cloud_message_type: 'FCM',
          };
          yield call($axios.post, URLS.REGISTER_TOKEN_URL, sendRequest);
          console.log('INSIDE REGISTER TOKEN');
          yield put({
            type: authActions.REGISTER_NOTIFICATION_TOKEN_SUCCESS,
            payload: sendData,
          });
          const response1 = yield call(loadStorageData, 'userData');
          const sendData1 = {
            ...response1,
            registerToken: data,
            isFirebaseTokenRegistered: true,
          };
          yield call(saveDataInStorage, 'userData', sendData1);
        }
      } catch (e) {
        if (e?.response?.data?.registration_id) {
          const response1 = yield call(loadStorageData, 'userData');
          const sendData1 = {
            ...response1,
            registerToken: data,
            isFirebaseTokenRegistered: e?.response?.data?.registration_id
              ? true
              : false,
          };
          yield call(saveDataInStorage, 'userData', sendData1);
          yield put({
            type: authActions.REGISTER_NOTIFICATION_TOKEN_FAILED,
          });
        }
      }
    },
  );
}

export function* resetPassword() {
  yield takeEvery(authActions.RESET_PASSWORD_REQUEST, function* ({data}) {
    try {
      const {resetData} = yield select(getAuthState);
      const sendData = {
        new_password1: data.password,
        new_password2: data.confirm,
        ...resetData,
      };
      yield call($axios.post, URLS.RESET_PASSWORD_URL, sendData);
      yield put({
        type: authActions.RESET_PASSWORD_SUCCESS,
        payload: true,
      });
      yield put({
        type: authActions.RESET_MESSAGE_REQUEST,
      });
      Toast('तपाईको पासवर्ड सफलतापूर्वक परिवर्तन भयो');
      RootNavigation.navigate('Login');
    } catch (error) {
      if (error?.response?.data?.token) {
        Toast('Token invalid. Failed to reset password');
      }
      yield put({
        type: authActions.RESET_PASSWORD_FAILED,
      });
    }
  });
}

export function* getResetUrl() {
  yield takeEvery(authActions.GET_RESET_URL_REQUEST, function* ({data}) {
    try {
      const splitResetURL = data.split('/');
      const sendData = {
        uid: splitResetURL[splitResetURL.length - 3],
        token: splitResetURL[splitResetURL.length - 2],
      };
      yield put({
        type: authActions.GET_RESET_URL_SUCCESS,
        payload: sendData,
      });
      RootNavigation.navigate('NewPassword');
    } catch (error) {
      yield put({
        type: authActions.GET_RESET_URL_FAILED,
      });
    }
  });
}

export function* changePassword() {
  yield takeEvery(authActions.CHANGE_PASSWORD_REQUEST, function* ({data}) {
    try {
      yield call($axios.post, URLS.CHANGE_PASSWORD_URL, data);
      yield put({
        type: authActions.CHANGE_PASSWORD_SUCCESS,
      });
      Toast('तपाईको पासवर्ड सफलतापूर्वक परिवर्तन भयो');
    } catch (error) {
      if (error?.response?.data?.old_password?.[0]) {
        Toast(
          'Your old password was entered incorrectly. Please enter it again.',
        );
      }
      if (
        error?.response?.data?.new_password1?.[0] ||
        error?.response?.data?.new_password2?.[0]
      ) {
        Toast(
          error?.response?.data?.new_password2?.[0] ||
            error?.response?.data?.new_password1?.[0],
        );
      }
      yield put({
        type: authActions.CHANGE_PASSWORD_FAILED,
      });
    }
  });
}

export default function* authSaga() {
  yield all([
    fork(logIn),
    fork(checkUserLogin),
    fork(signUp),
    fork(verifyEmail),
    fork(logOut),
    fork(forgotPassword),
    fork(resetPassword),
    fork(changePassword),
    fork(getResetUrl),
    fork(userInfo),
    fork(registerTokenFCM),
  ]);
}
