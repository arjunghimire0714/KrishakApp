import {networkSaga} from 'react-native-offline';
import {all, fork} from 'redux-saga/effects';
import authSagas from './auth';

export default function* rootSaga() {
  yield all([fork(authSagas), fork(networkSaga, {pingInterval: 20000})]);
}
