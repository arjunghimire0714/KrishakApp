import {createSlice} from '@reduxjs/toolkit';
import {authActions} from '../actions';
import isEmpty from 'lodash/isEmpty';

const authSlice = createSlice({
  name: 'auth',
  initialState: {
    isLoggedUser: false,
    checkLoggedUser: false,
    authLoading: false,
    signUpData: {},
    userDetails: {},
    loginStatus: undefined,
    message: undefined,
    forgotEmail: '',
    resetData: {},
    isResetSuccess: false,
    changePasswordLoading: false,
  },

  extraReducers: {
    [authActions.CHECK_LOGIN_REQUEST]: state => {
      state.authLoading = true;
      state.checkLoggedUser = true;
    },
    [authActions.CHECK_LOGIN_SUCCESS]: (state, action) => {
      state.authLoading = false;
      state.isLoggedUser = isEmpty(action.payload) ? false : true;
      state.checkLoggedUser = false;
      state.userDetails = action.payload;
    },
    [authActions.CHECK_LOGIN_FAILED]: state => {
      state.authLoading = false;
      state.checkLoggedUser = false;
    },

    [authActions.LOGIN_REQUEST]: state => {
      state.authLoading = true;
      state.message = undefined;
    },
    [authActions.LOGIN_SUCCESS]: (state, action) => {
      state.authLoading = false;
      state.isLoggedUser = isEmpty(action.payload) ? false : true;
      state.userDetails = action.payload;
      state.message = undefined;
    },
    [authActions.LOGIN_FAILED]: (state, action) => {
      state.authLoading = false;
      state.message = action.payload;
    },

    [authActions.LOGOUT_REQUEST]: state => {
      state.authLoading = true;
    },
    [authActions.LOGOUT_SUCCESS]: (state, action) => {
      state.authLoading = false;
      state.isLoggedUser = false;
      state.userDetails = action.payload;
    },
    [authActions.LOGOUT_FAILED]: state => {
      state.authLoading = false;
    },

    [authActions.REGISTER_REQUEST]: state => {
      state.authLoading = true;
    },
    [authActions.REGISTER_SUCCESS]: (state, action) => {
      state.authLoading = false;
      state.signUpData = action.payload;
    },
    [authActions.REGISTER_FAILED]: state => {
      state.authLoading = false;
    },
    [authActions.FORGOT_PASSWORD_REQUEST]: state => {
      state.authLoading = true;
    },
    [authActions.FORGOT_PASSWORD_SUCCESS]: (state, action) => {
      state.authLoading = false;
      state.message = action.payload;
    },
    [authActions.FORGOT_PASSWORD_FAILED]: (state, action) => {
      state.authLoading = false;
      state.message = action.payload;
    },

    [authActions.GET_RESET_URL_REQUEST]: state => {
      state.authLoading = true;
    },
    [authActions.GET_RESET_URL_SUCCESS]: (state, action) => {
      state.authLoading = false;
      state.resetData = action.payload;
    },
    [authActions.GET_RESET_URL_FAILED]: state => {
      state.authLoading = false;
    },
    [authActions.RESET_PASSWORD_REQUEST]: state => {
      state.authLoading = true;
    },
    [authActions.RESET_PASSWORD_SUCCESS]: (state, action) => {
      state.authLoading = false;
      state.isResetSuccess = true;
    },
    [authActions.RESET_PASSWORD_FAILED]: state => {
      state.authLoading = false;
    },

    [authActions.CHANGE_PASSWORD_REQUEST]: state => {
      state.changePasswordLoading = true;
    },
    [authActions.CHANGE_PASSWORD_SUCCESS]: (state, action) => {
      state.changePasswordLoading = false;
    },
    [authActions.CHANGE_PASSWORD_FAILED]: state => {
      state.changePasswordLoading = false;
    },

    [authActions.USER_INFO_REQUEST]: state => {
      state.authLoading = true;
    },
    [authActions.USER_INFO_SUCCESS]: (state, action) => {
      state.authLoading = false;
      state.userDetails = action.payload;
    },
    [authActions.USER_INFO_FAILED]: state => {
      state.authLoading = false;
    },

    [authActions.RESET_MESSAGE_REQUEST]: state => {
      state.message = undefined;
    },

    [authActions.SET_NOTIFICATION_TOKEN_SUCCESS]: (state, action) => {
      state.userDetails = action.payload;
    },
    [authActions.REGISTER_NOTIFICATION_TOKEN_REQUEST]: state => {},
    [authActions.REGISTER_NOTIFICATION_TOKEN_SUCCESS]: (state, action) => {
      state.userDetails = action.payload;
    },
    [authActions.REGISTER_NOTIFICATION_TOKEN_FAILED]: state => {},
  },
});

export default authSlice.reducer;
