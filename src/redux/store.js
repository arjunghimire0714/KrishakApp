import {combineReducers, configureStore} from '@reduxjs/toolkit';
import {
  createNetworkMiddleware,
  reducer as network,
} from 'react-native-offline';
import createSagaMiddleware from 'redux-saga';
import {authActions} from './actions';
import rootSaga from './sagas';
import authReducer from './slices/authSlice';

const sagaMiddleware = createSagaMiddleware();
const networkMiddleware = createNetworkMiddleware({
  queueReleaseThrottle: 200,
});

const appReducer = combineReducers({
  auth: authReducer,
  network,
});

const rootReducer = (state, action) => {
  if (action.type === authActions.LOGIN_SUCCESS) {
    return appReducer(undefined, action);
  }
  return appReducer(state, action);
};

const store = configureStore({
  reducer: rootReducer,
  middleware: [
    networkMiddleware,
    // ...getDefaultMiddleware({thunk: false, serializableCheck: false}),
    sagaMiddleware,
  ],
});
sagaMiddleware.run(rootSaga);

store.dispatch({type: authActions.CHECK_LOGIN_REQUEST});

export default store;
