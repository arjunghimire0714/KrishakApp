import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';

const storage = new Storage({
  size: 1000,
  storageBackend: AsyncStorage,
  defaultExpires: null,
  enableCache: true,
});

const saveDataInStorage = (key, data) =>
  storage.save({
    key,
    data,
    expires: null,
  });

const loadStorageData = key =>
  storage
    .load({
      key,
      autoSync: true,
      syncInBackground: true,
    })
    .then(response => {
      return response;
    })
    .catch(err => {
      switch (err.name) {
        case 'NotFoundError':
          saveDataInStorage(key, null);
          break;
      }
    });

const removeStorageData = key =>
  storage.remove({
    key,
  });

export {saveDataInStorage, loadStorageData, removeStorageData};
