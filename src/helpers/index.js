import {Dimensions} from 'react-native';
import moment from 'moment';
// import 'moment/locale/ne';

// moment.locale('ne', {
//   // months:
//   //   'वैशाख_जेठ_असार_साउन_भदौ_असोज_कार्तिक_मंसिर_पुष_माघ_फागुन_फागुन'.split('_'),
//   // monthsParseExact: true,
//   weekdays: 'आइतवार_सोमवार_मङ्गलवार_बुधवार_बिहिवार_शुक्रवार_शनिवार'.split('_'),
//   weekdaysParseExact: true,
// });

export const days = {
  sunday: 'आइतबार',
  monday: 'सोमबार',
  tuesday: 'मङ्गलबार',
  wednesday: 'बुधबार',
  thursday: 'बिहिबार',
  friday: 'शुक्रबार',
  saturday: 'शनिबार',
};

export const times = {
  am: 'बिहान',
  pm: 'दिउसो',
};

// export const numbers = {
//   1: '१',
//   2: '२',
//   3: '३',
//   4: '४',
//   5: '५',
//   6: '६',
//   7: '७',
//   8: '८',
//   9: '९',
//   0: '०',
// };

export {moment};
export const {width, height} = Dimensions.get('window');

export const webClientId =
  '35870594006-2mg5prjh6kf5l0pntr2t6mpm2ghbchcc.apps.googleusercontent.com';

export const sdkKey = 'F25RSgHbb0LUL468CwbJqu6BEltLUUqTQAqJ';
export const sdkSecret = 'TTjLKQCxRxcHz652BHKoliSAlrOSGSGKwO8D';

export const apiKey = 'RyDU-LxvSfipV9I6gWNcJg';
export const apiSecret = '0cYNzZ9svjJVjaUssdoo5YExM9cOqMKxFHuB';

export const appId = '958909811674969';

export const extractDataFromJoinLink = link => {
  const url = new URL(link);

  const password = url.searchParams.get('pwd');
  const meetingNumber = url.pathname.replace('/j/', '') || null;

  return {
    password,
    meetingNumber,
  };
};
