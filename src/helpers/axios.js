import axios from 'axios';
import {loadStorageData, saveDataInStorage} from './storage';
import {BASE_URL, URLS, NO_TOKEN_URLS} from './urls';

const getTokenFromStorage = async () => {
  return await loadStorageData('userData');
};

export const $axios = axios.create({
  baseURL: BASE_URL,
  timeout: 1000,
});

const refreshAccessToken = async () => {
  try {
    const userData = await loadStorageData('userData');
    if (userData?.refresh) {
      const response = await $axios.post(URLS.REFRESH_TOKEN_URL, {
        refresh: userData.refresh,
      });
      saveDataInStorage('userData', {
        ...userData,
        ...response.data,
      });
      $axios.defaults.headers.common[
        'Authorization'
      ] = `Bearer ${response.access}`;
      return response.access;
    } else {
      return null;
    }
  } catch (error) {
    console.log('REFRESH TOKEN ERROR', error);
  }
};

$axios.interceptors.request.use(
  async config => {
    if (!NO_TOKEN_URLS.includes(config.url)) {
      const {access} = await getTokenFromStorage();
      config.headers = {
        Authorization: `Bearer ${access}`,
      };
    }
    return config;
  },
  error => {
    Promise.reject(error);
  },
);

$axios.interceptors.response.use(
  response => {
    return response;
  },
  async function (error) {
    const originalRequest = error.config;
    console.log('RESPONSE INTER ERROR', error?.response);
    if (error?.response?.status === 401 && !originalRequest?._retry) {
      originalRequest._retry = true;
      const access = await refreshAccessToken();
      axios.defaults.headers.common['Authorization'] = 'Bearer ' + access;
      return $axios(originalRequest);
    }
    return Promise.reject(error);
  },
);
