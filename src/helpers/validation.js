import * as Yup from 'yup';

const phoneRegExp = /(?:\+977[- ])?\d{2}-?\d{8}/gm;

export const loginSchema = Yup.object().shape({
  email: Yup.string()
    .email('Invalid email address')
    .required('Email field is required'),
  password: Yup.string()
    .required('Password field is required')
    .min(8, 'Password is too short - should be 8 chars minimum.'),
});

export const signupSchema = Yup.object({
  name: Yup.string().required('Email field is required'),
  email: Yup.string()
    .email('Invalid email address')
    .required('Email field is required'),

  password: Yup.string()
    .required('Password field is required')
    .min(8, 'Password is too short - should be 8 chars minimum.'),
  // confirm: Yup.string()
  //   .when('password', {
  //     is: val => (val && val.length > 0 ? true : false),
  //     then: Yup.string().oneOf(
  //       [Yup.ref('password')],
  //       'Both password need to be the same',
  //     ),
  //   })
  //   .required('Password field is required'),
});

export const newPasswordSchema = Yup.object({
  password: Yup.string()
    .required('पासवर्ड फिल्ड आवश्यक छ')
    .min(8, 'पासवर्ड धेरै छोटो छ - न्यूनतम 8 वर्ण हुनुपर्छ।'),
  confirm: Yup.string()
    .when('password', {
      is: val => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref('password')],
        'तपाईले दोहोर्याउनुभएको पासवर्ड मिलेन',
      ),
    })
    .required('नयाँ पासवर्ड फिल्ड आवश्यक छ'),
});

export const forgotPasswordSchema = Yup.object({
  email: Yup.string()
    .email('Invalid email address')
    .required('Email field is required'),
});

export const changePasswordSchema = Yup.object({
  old_password: Yup.string().required('Old password field is required'),
  new_password1: Yup.string()
    .required('New password field is required')
    .min(8, 'Password is too short - should be 8 chars minimum.'),
  new_password2: Yup.string()
    .when('new_password1', {
      is: val => (val && val.length > 0 ? true : false),
      then: Yup.string().oneOf(
        [Yup.ref('new_password1')],
        'Both password need to be the same',
      ),
    })
    .required('Confirm password field is required'),
});

export const trainingSchema = Yup.object({
  name: Yup.string().required('नाम फिल्ड आवश्यक छ'),
  province: Yup.string().required('प्रदेश फिल्ड आवश्यक छ'),
  district: Yup.string().required('जिल्ला फिल्ड आवश्यक छ'),
  body: Yup.string().required('निकाय फिल्ड आवश्यक छ'),
  ward_no: Yup.string().required('वडा नं फिल्ड आवश्यक छ'),
  profession: Yup.array().min(1, 'पेशा फिल्ड आवश्यक छ').required(),
  contact_no: Yup.string()
    .matches(phoneRegExp, 'फोन नम्बर मान्य छैन')
    .required('फोन नम्बर आवश्यक छ')
    .test(
      'len',
      ' फोन नम्बर ठ्याक्कै १० नम्बर हुनुपर्छ',
      val => val?.length === 10,
    ),
});

// पेशा क्षेत्रमा कम्तिमा १ सेलेक्ट हुनुपर्छ
