export const BASE_URL = 'https://skapi.offgridbazaar.com/api/v1/';

export const URLS = {
  LOGIN_URL: 'users/jwt/token/',
  SIGNUP_URL: 'users/registration/',
  REFRESH_TOKEN_URL: 'users/jwt/refresh/',
  VERIFY_URL: 'users/verify-email/',
  RESENT_EMAIL: 'users/resend/verificatin-email/',
  GET_USER_URL: 'users/detail/',
  FORGOT_PASSWORD_URL: 'users/password-reset/',
  RESET_PASSWORD_URL: 'users/password-reset/confirm/',
  GOOGLE_REGISTRATION_URL: 'users/google/registration/',
  FACEBOOK_REGISTRATION_URL: 'users/facebook/registration/',
  INVITE_REGISTRATION_URL: 'sutdent/invite/',
  CHANGE_PASSWORD_URL: 'users/change-password/',

  GET_ONGOING_TRAININGS_URL: searchQuery =>
    searchQuery ? `trainings/ongoing/${searchQuery}` : 'trainings/ongoing/',

  GET_UPCOMING_TRAININGS_URL: searchQuery =>
    searchQuery ? `trainings/upcoming/${searchQuery}` : 'trainings/upcoming/',
  GET_OLD_TRAININGS_URL: searchQuery =>
    searchQuery
      ? `trainings/old/training-series/${searchQuery}`
      : 'trainings/old/training-series/',

  GET_OLD_TRAININGS_SERIES_URL: series => `trainings/by-series/${series}/`,
  TRAINING_REGISTRATION_URL: trainingId =>
    `trainings/registration/${trainingId}/`,

  GET_ARTICLES_URL: searchQuery =>
    searchQuery ? `articles/${searchQuery}` : 'articles/',
  GET_ARTICLE_URL: id => `articles/${id}`,
  GET_ARTICLE_TAGS_URL: 'articles/tags/',
  GET_ARTICLE_TAGS_BY_SLUG_URL: slug => `articles/tag/${slug}/`,
  SET_ARTICLES_REACTIONS_URL: articleId => `articles/reactions/${articleId}/`,

  GET_QUIZZES: 'quiz/ongoing_quiz/',

  GET_QUIZ_LEADERBOARD_URL: (quizId, searchQuery) =>
    searchQuery
      ? `quiz/leader-board/${quizId}/${searchQuery}`
      : `quiz/leader-board/${quizId}/`,

  SUBMIT_QUIZ_URL: quizId => `quiz/submit/${quizId}/`,
  GET_NOTIFICATIONS_URL: searchQuery =>
    searchQuery
      ? `notifications/notifications/${searchQuery}`
      : 'notifications/notifications/',

  REGISTER_TOKEN_URL: 'devices/gcm/',

  REVOKE_REGISTRATION_TOKEN_URL: registrationId =>
    `devices/gcm/${registrationId}/`,

  GET_QUIZ_WEEKLY_LEADERBOARD_URL: 'quiz/weekly/leader-board/',
};

export const NO_TOKEN_URLS = [
  URLS.LOGIN_URL,
  URLS.SIGNUP_URL,
  URLS.RESENT_EMAIL,
  URLS.VERIFY_URL,
  URLS.FORGOT_PASSWORD_URL,
  URLS.RESET_PASSWORD_URL,
  URLS.GOOGLE_REGISTRATION_URL,
  URLS.FACEBOOK_REGISTRATION_URL,
  URLS.INVITE_REGISTRATION_URL,
];
