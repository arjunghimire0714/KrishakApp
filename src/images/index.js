import GoogleLogin from './google-login.svg';
import FacebookLogin from './facebook-login.svg';
import PasswordShowEye from './password-show-eye.svg';
import PasswordHideEye from './password-hide-eye.svg';
import BackArrow from './back-arrow.svg';
import SearchIcon from './search.svg';
import FacebookIcon from './facebook.svg';
import MessengerIcon from './messenger.svg';
import WhatsappIcon from './whatsapp.svg';
import ViberIcon from './viber.svg';
import TimerIcon from './time.svg';
import CorrectIcon from './correct.svg';
import InCorrectIcon from './incorrect.svg';
import TrainingSuccessIcon from './training-success.svg';
import ContestsIcon from './contests.svg';
import TrainingIcon from './training.svg';
import SuggestionIcon from './suggestion.svg';
import HomeIcon from './tab/home.svg';
import ProfileIcon from './tab/profile.svg';
import NotificationsIcon from './tab/notifications.svg';
import HomeActiveIcon from './tab/home_active.svg';
import ProfileActiveIcon from './tab/profile_active.svg';
import NotificationsActiveIcon from './tab/notifications_active.svg';
import TwentyCoinIcon from './20coin.svg';
import LogoSvg from './logo.svg';
import CoinUpSvg from './coins/coin_up.svg';
import PlusSvg from './plus.svg';
import MinusSvg from './minus.svg';

import FirstCoin from './coins/first.svg';
import FirstCoinB from './coins/first_b.svg';
import SecondCoin from './coins/second.svg';
import SecondCoinB from './coins/second_b.svg';
import FifthCoin from './coins/fifth.svg';
import FifthCoinB from './coins/fifth_b.svg';
import TenthCoin from './coins/tenth.svg';
import TenthCoinB from './coins/tenth_b.svg';
import FifteenCoin from './coins/fifteen.svg';
import FifteenCoinB from './coins/fifteen_b.svg';
import TwentyCoin from './coins/twenty.svg';
import TwentyCoinB from './coins/twenty_b.svg';
import TwentyFiveCoin from './coins/twentyfive.svg';
import TwentyFiveCoinB from './coins/twentyfive_b.svg';
import FiftyCoin from './coins/fifty.svg';
import FiftyCoinB from './coins/fifty_b.svg';
import HundredCoin from './coins/hundred.svg';
import HundredCoinB from './coins/hundred_b.svg';
import FiveHundredCoin from './coins/fivehundred.svg';
import FiveHundredCoinB from './coins/fivehundred_b.svg';
import ThousandCoin from './coins/thousand.svg';
import ThousandCoinB from './coins/thousand_b.svg';

export {
  GoogleLogin,
  FacebookLogin,
  PasswordShowEye,
  PasswordHideEye,
  BackArrow,
  SearchIcon,
  FacebookIcon,
  MessengerIcon,
  WhatsappIcon,
  ViberIcon,
  TimerIcon,
  TrainingSuccessIcon,
  ContestsIcon,
  TrainingIcon,
  SuggestionIcon,
  HomeIcon,
  ProfileIcon,
  NotificationsIcon,
  HomeActiveIcon,
  ProfileActiveIcon,
  NotificationsActiveIcon,
  CorrectIcon,
  InCorrectIcon,
  TwentyCoinIcon,
  LogoSvg,
  CoinUpSvg,
  PlusSvg,
  MinusSvg,
  FirstCoin,
  FirstCoinB,
  SecondCoin,
  SecondCoinB,
  FifthCoin,
  FifthCoinB,
  TenthCoin,
  TenthCoinB,
  FifteenCoin,
  FifteenCoinB,
  TwentyCoin,
  TwentyCoinB,
  TwentyFiveCoin,
  TwentyFiveCoinB,
  FiftyCoin,
  FiftyCoinB,
  HundredCoin,
  HundredCoinB,
  FiveHundredCoin,
  FiveHundredCoinB,
  ThousandCoin,
  ThousandCoinB,
};
